#!/usr/bin/python

import cv2  # pip install opencv-python
# importing some useful packages
import numpy as np
import math
from math import acos, degrees, atan, atan2
import os
import sys
from matplotlib import pyplot as plt


# Todo: add roi before file_holes
# Todo: use contour_feature_fitLine/minAreaRect/minEnclosingTriangle to find slope theta
# Todo: use blur_bilateralFilter/blur_adaptiveBilateralFilter before canny/threshold/adaptativeThreshold
# Todo: try blob, refer https://www.learnopencv.com/blob-detection-using-opencv-python-c/
# Todo: remove shadow https://blog.csdn.net/hust_bochu_xuchao/article/details/54019994


class image_tool:

    def log(self, msg, obj=None):
        np.set_printoptions(threshold=np.nan) # show full content of matrix for "print" or "savetxt"
        if self.debug:
            if obj is not None:
                msg = str(msg) + ' '+ str(obj)
            print msg

    def __init__(self, debug=False):
        self.input_file_name = None
        self.version = None
        self.output_file_name = None
        self.debug = debug
        self.log('file_name:', self.output_file_name)
        self.log('debug:', self.debug)
        self.output_file_name_sequence = 0

    def imread(self, file_name):
        self.input_file_name = os.path.basename(file_name).split(".")[0]
        self.output_file_name = self.input_file_name + ( '' if self.version is None else '-' + self.version )
        return cv2.imread(file_name)

    def set_version(self, version):
        self.version = version
        self.output_file_name = ( '' if self.input_file_name is None else self.input_file_name + '-' ) + self.version

    def save(self, operation, img, saveRequired=False):
        if self.debug or saveRequired:
            if operation is not None:
                self.output_file_name = ( '' if self.output_file_name is None else self.output_file_name + '-' ) + operation
                cv2.imwrite(self.output_file_name + '.png', img)
            else:
                self.output_file_name_sequence+=1
                output_file_name_temp = ( '' if self.output_file_name is None else self.output_file_name + '-' ) + str(self.output_file_name_sequence)
                cv2.imwrite(output_file_name_temp + '.png', img)
        return img

    def savetxt(self, channel):
        np.set_printoptions(threshold=np.nan) # show full content of matrix for "print" or "savetxt"
        if self.debug:
            np.savetxt(self.output_file_name + '.png.txt', np.array(channel), fmt="%s")

    def hist(self, grayscaled):
        hist_full = cv2.calcHist([grayscaled],[0],None,[256],[0,256])
        plt.plot(hist_full)
        plt.xlim([0,256])

        if self.debug:
            self.output_file_name += '-hist'
            # plt.show()
            plt.savefig(self.output_file_name + '.png')
        return hist_full

    def unique(self, propertyName, channel):
        print propertyName+ " unique values are:" + str(np.unique(channel))

    def max(self, propertyName, channel):
        print propertyName + " max value is:" + str(np.max(channel))

    def bitwise_and(self, gray_image, mask_yw, operation='and'):
        return self.save(operation, cv2.bitwise_and(gray_image, mask_yw))

    def bitwise_or(self, mask_white, mask_yellow, operation='or'):
        return self.save(operation, cv2.bitwise_or(mask_white, mask_yellow))

    def inRange(self, img_hsv, lower_yellow, upper_yellow, operation='range'):
        return self.save(operation, cv2.inRange(img_hsv, lower_yellow, upper_yellow))

    # Color space
    # https://www.geeksforgeeks.org/python-visualizing-image-in-different-color-spaces/
    # https://en.wikipedia.org/wiki/HSL_and_HSV
    # https://stackoverflow.com/questions/48795918/how-to-save-hsv-converted-image-in-python
    # http://answers.opencv.org/question/99162/had-problem-when-convert-the-image-from-bgr-to-hsv/

    def rgb2hsv(self, img, operation='hsv'):
        # hsv = [hue, saturation, value]
        # more accurate range for yellow since it is not strictly black, white, r, g, or b
        # H(0-179) S/V(0-255)
        return self.save(operation, cv2.cvtColor(img, cv2.COLOR_BGR2HSV))

    def hsv2rgb(self, img, operation='hsv2rgb'):
        # hsv = [hue, saturation, value]
        # more accurate range for yellow since it is not strictly black, white, r, g, or b
        return self.save(operation, cv2.cvtColor(img, cv2.COLOR_HSV2BGR))

    def rgb2gray(self, img, operation='gray'):
        """Applies the Grayscale transform
        This will return an image with only one color channel
        but NOTE: to see the returned image as grayscale
        (assuming your grayscaled image is called 'gray')
        you should call plt.imshow(gray, cmap='gray')"""
        return self.save(operation, cv2.cvtColor(img, cv2.COLOR_BGR2GRAY))
        # Or use BGR2GRAY if you read an image with cv2.imread()
        # return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    def get_dominant_color(image):
        average_color = np.mean(image).astype(np.uint8)
        return average_color

    def split(self, img):
        return cv2.split(img)

    def merge(self, channels, operation='merge'):
        return self.save(operation, cv2.merge(channels))

    def canny(self, img, low_threshold, high_threshold, apertureSize=3, L2gradient=False, operation='canny'):
        """Applies the Canny transform"""
        return self.save(operation, cv2.Canny(img, low_threshold, high_threshold, apertureSize=apertureSize, L2gradient=L2gradient))

    ### Blur

    # https://docs.opencv.org/3.1.0/d4/d13/tutorial_py_filtering.html

    def blur_filter2D(self, img):
        kernel = np.ones((5,5), np.float32)/25
        dst = cv2.filter2D(img,-1,kernel)
        return dst

    def blur_gaussian(self, img, kernel_size, operation='blur'):
        """Applies a Gaussian Noise kernel"""
        return self.save(operation, cv2.GaussianBlur(img, (kernel_size, kernel_size), 0))

    def blur_median(self, channel, ksize=13):
        img2 = cv2.medianBlur(channel, ksize)
        return img2

    def blur_bilateralFilter(self, img, d=9, sigmaColor=75, sigmaSpace=75 ):
        blur = cv2.bilateralFilter(img, d, sigmaColor, sigmaSpace)
        return blur

    def blur_adaptiveBilateralFilter(self, img, ksize=9, sigmaSpace=75, maxSigmaColor=75,  ):
        blur = cv2.adaptiveBilateralFilter(img, ksize, sigmaSpace, maxSigmaColor=maxSigmaColor)
        return blur

    # used for detect blur, blur measure, focus measure, variance_of_laplacian
    # thresh=100: focus measures that fall below this value will be considered 'blurry'
    # https://blog.csdn.net/jacke121/article/details/80574451
    def blur_measure(self,gray):
        # compute the Laplacian of the image and then return the focus
        # measure, which is simply the variance of the Laplacian
        return cv2.Laplacian(gray, cv2.CV_64F).var()

    def sharpen_laplace(self, image, operation="laplace"):
        kernel = np.array([
            [0, -1, 0],
            [-1, 5, -1],
            [0, -1, 0]], dtype=np.float32)
        dst = cv2.filter2D(image, -1, kernel)
        return self.save(operation, dst)

    # https://blog.csdn.net/akadiao/article/details/79679306
    # def sharpen_gamma(self, image, y=1.5, operation="gamma"):
    #     return self.save(operation, np.power(image/float(np.max(image)), y))
    def sharpen_gamma(self, img, gamma, operation="gamma"):
        gamma_table = [np.power(x/255.0, gamma)*255.0 for x in range(256)]
        gamma_table = np.round(np.array(gamma_table)).astype(np.uint8)
        return self.save(operation, cv2.LUT(img, gamma_table))

    ### Draw

    def region_of_interest(self, img, vertices, operation='roi'):
        """
        Applies an image mask.

        Only keeps the region of the image defined by the polygon
        formed from `vertices`. The rest of the image is set to black.
        """
        # defining a blank mask to start with
        mask = np.zeros_like(img)

        # defining a 3 channel or 1 channel color to fill the mask with depending on the input image
        if len(img.shape) > 2:
            channel_count = img.shape[2]  # i.e. 3 or 4 depending on your image
            ignore_mask_color = (255,) * channel_count
        else:
            ignore_mask_color = 255

        # filling pixels inside the polygon defined by "vertices" with the fill color
        cv2.fillPoly(mask, vertices, ignore_mask_color)

        # returning the image only where mask pixels are nonzero
        masked_image = cv2.bitwise_and(img, mask)
        return self.save(operation, masked_image)

    def draw_lines(self, img, lines, color=[0, 0, 255], thickness=2, operation='draw'):
        """
        NOTE: this is the function you might want to use as a starting point once you want to
        average/extrapolate the line segments you detect to map out the full
        extent of the lane (going from the result shown in raw-lines-example.mp4
        to that shown in P1_example.mp4).

        Think about things like separating line segments by their
        slope ((y2-y1)/(x2-x1)) to decide which segments are part of the left
        line vs. the right line.  Then, you can average the position of each of
        the lines and extrapolate to the top and bottom of the lane.

        This function draws `lines` with `color` and `thickness`.
        Lines are drawn on the image inplace (mutates the image).
        If you want to make the lines semi-transparent, think about combining
        this function with the weighted_img() function below
        """
        if lines is not None:
            for line in lines:
                for x1, y1, x2, y2 in line:
                    cv2.line(img, (x1, y1), (x2, y2), color, thickness)

    def hough_lines(self, img, rho, theta, threshold, min_line_len, max_line_gap, operation='hough'):
        """
        `img` should be the output of a Canny transform.

        Returns an image with hough lines drawn.
        """
        lines = cv2.HoughLinesP(img, rho, theta, threshold, np.array([]), minLineLength=min_line_len,
                                maxLineGap=max_line_gap)
        line_img = np.zeros((img.shape[0], img.shape[1], 3), dtype=np.uint8)
        self.draw_lines(line_img, lines)
        return self.save(operation, line_img)

    # Python 3 has support for cool math symbols.

    def weighted_img(self, img, initial_img, a=0.8, b=1., r=0., operation='weight'):
        """
        `img` is the output of the hough_lines(), An image with lines drawn on it.
        Should be a blank image (all black) with lines drawn on it.

        `initial_img` should be the image before any processing.

        The result image is computed as follows:

        initial_img * a + img * b + r
        NOTE: initial_img and img must be the same shape!
        """
        return self.save(operation, cv2.addWeighted(initial_img, a, img, b, r))

    ### threshold

    def mask_to_yellow(self, img_hsv, operation='rangeYellow'):
        # hsv = [hue, saturation, value]
        # more accurate range for yellow since it is not strictly black, white, r, g, or b
        lower_yellow = np.array([20, 100, 100], dtype="uint8")
        upper_yellow = np.array([30, 255, 255], dtype="uint8")
        mask_yellow = self.inRange(img_hsv, lower_yellow, upper_yellow, operation=None)
        return self.save(operation,  mask_yellow)

    def mask_to_white(self, gray_image, operation='rangeWhite'):
        return self.save(operation,   self.inRange(gray_image, 140, 255, operation=None))

    def mask_to_yellow_or_white(self, gray_image, img_hsv, operation='YellowOrWhite'):
        mask_yellow = self.mask_to_yellow(img_hsv)
        mask_white = self.mask_to_white(gray_image)
        mask_yw = self.bitwise_or(mask_white, mask_yellow, operation=None)
        return self.save(operation, mask_yw)

    def threshold(self, grayscaled, threshold_value=150, max_value=255, type=cv2.THRESH_BINARY, operation='thresh'):
        ret3,threshold = cv2.threshold(grayscaled, threshold_value, max_value, type)
        return self.save(operation,  threshold)

    # Otsu's thresholding after Gaussian filtering
    def threshold_otsu(self, grayscaled, threshold_value=150, max_value=255, type=cv2.THRESH_BINARY+cv2.THRESH_OTSU, operation='threshOTSU'):
        return self.threshold(grayscaled, threshold_value, max_value, type, operation)

    # th1 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,5)
    # th2 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,5)
    def threshold_adaptive(self, grayscaled, blockSize=29, constant=1, max_value=255, type=cv2.THRESH_BINARY, adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C, operation='adaptiveThresh'):
        operation+=(str(blockSize) + ',' + str(constant))
        threshold = cv2.adaptiveThreshold(grayscaled, max_value, adaptiveMethod, type, blockSize, constant)
        return self.save(operation,  threshold)

    # http://www.cnblogs.com/polly333/p/7269153.html
    # image7_text.png, image6_text.png
    # Derek Bradley and Gerhard Roth  W*W
    # global( Hist+threshold < adaptive ) <
    def threshold_integral(self, gray_image,  sub_thresh = 0.15, win_length=None, operation='theshIntegral'):
        integralimage = cv2.integral(gray_image, cv2.CV_32F)

        width = gray_image.shape[1]
        height = gray_image.shape[0]
        if win_length is None:
            win_length = int(width / 10)

        image_thresh = np.zeros((height, width, 1), dtype = np.uint8)
        # perform threshholding
        for j in range(height):
            for i in range(width):

                x1 = i - win_length
                x2 = i + win_length
                y1 = j - win_length
                y2 = j + win_length

                # check the border
                if(x1 < 0):
                    x1 = 0
                if(y1 < 0):
                    y1 = 0
                if(x2 > width):
                    x2 = width -1
                if(y2 > height):
                    y2 = height -1
                count = (x2- x1) * (y2 - y1)

                #            I(x,y) = s(x2,y2) - s(x1,y2) - s(x2, y1) + s(x1, y1)
                sum = integralimage[y2, x2] - integralimage[y1, x2] -integralimage[y2, x1] + integralimage[y1, x1]
                if (int)(gray_image[j, i] * count) < (int) (sum * (1.0 - sub_thresh)):
                    image_thresh[j, i] = 0
                else:
                    image_thresh[j, i] = 255

        return self.save(operation,  image_thresh)

    ### morph

    # https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_morphological_ops/py_morphological_ops.html
    # clean noise from background (black, 0)
    def morph_open(self, grayscaled, kernel_size=(7,7), morph_type=cv2.MORPH_OPEN, operation='open'):
        morph = grayscaled.copy()
        #kernel = np.ones((7, 7), np.uint8)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernel_size)
        morph = cv2.morphologyEx(morph, morph_type, kernel)
        return self.save(operation,  morph)

    def morph_dilate(self, grayscaled, kernel_size=(7,7), iterations = 1, operation='dilate'):
        morph = grayscaled.copy()
        #kernel = np.ones((7, 7), np.uint8)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernel_size)
        morph = cv2.dilate(morph, kernel, iterations)
        return self.save(operation,  morph)

    def morph_erode(self, grayscaled, kernel_size=(7,7), iterations = 1, operation='erode'):
        morph = grayscaled.copy()
        #kernel = np.ones((7, 7), np.uint8)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernel_size)
        morph = cv2.erode(morph, kernel, iterations)
        return self.save(operation,  morph)

    # clean noise from foreground (white, 255)
    # connect cutted line
    def morph_close(self, grayscaled, kernel_size=(7,7), operation='close'):
        return self.morph_open(grayscaled, kernel_size, cv2.MORPH_CLOSE, operation)

    # only h/w, not channel
    # [startIndex:excluded_endIndex:step], revert [::-1], even [0::2]
    def get_shape(self, image):
        # h, w, chn = image.shape
        h, w = image.shape[0:2]
        return h, w

    def flood_fill(self, image, loDiff=(3,)*3, upDiff=(3,)*3, seed=None, isFixed=False, operation='flood'):
        shape = image.shape
        h=shape[0]
        w=shape[1]

        mask = np.zeros((h+2,w+2),np.uint8)

        if seed is None:
            seed = (w/2,h-5)

        floodflags = 4
        floodflags |= cv2.FLOODFILL_MASK_ONLY
        floodflags |= (255 << 8)
        if isFixed:
            floodflags |= cv2.FLOODFILL_FIXED_RANGE

        num,image,mask,rect = cv2.floodFill(image, mask, seed, (255,0,0), loDiff, upDiff, floodflags)
        return self.save(operation,  mask)

    def flood_fill_fixed(self, image, loDiff=(10,)*3, upDiff=(10,)*3, seed=None, operation='floodFix'):
        return self.flood_fill(image, loDiff, upDiff, seed, True, operation)

    # mask: background black, forground white
    def fill_holes(self, mask, operation='fill'):
        ### 8. Filling holes in an image
        # https://www.learnopencv.com/filling-holes-in-an-image-using-opencv-python-c/

        # Threshold.
        # Set values equal to or above 220 to 0.
        # Set values below 220 to 255.
        th, im_th = cv2.threshold(mask, 220, 255, cv2.THRESH_BINARY_INV);
        # self.save('im_th',  im_th)
        # Copy the thresholded image.
        im_floodfill = im_th.copy()
        h,w = im_floodfill.shape
        im_floodfill[0:1,0:w+1]=255
        # im_floodfill = self.edge_add(im_floodfill, edge_value=255)
        # mask = self.edge_add(mask, edge_value=0)
        # Mask used to flood filling.
        # Notice the size needs to be 2 pixels than the image.
        mask_mask = np.zeros((h+2, w+2), np.uint8)
        # Floodfill from point (0, 0)

        # self.save('im_floodfill_before',  im_floodfill)
        # self.savetxt(im_floodfill)
        cv2.floodFill(im_floodfill, mask_mask, (0,0), (0,0,0));
        # self.save('fill_holes_mask',  im_floodfill)
        # Combine the two images to get the foreground.
        mask_result = mask | im_floodfill
        return self.save(operation,  mask_result)

    ### contours
    # https://docs.opencv.org/3.1.0/dd/d49/tutorial_py_contour_features.html
    # https://docs.opencv.org/2.4/modules/core/doc/drawing_functions.html
    # https://docs.opencv.org/3.1.0/d3/dc0/group__imgproc__shape.html
    # https://docs.opencv.org/3.0-beta/modules/imgproc/doc/structural_analysis_and_shape_descriptors.html
    # https://docs.opencv.org/3.4/d3/dc0/group__imgproc__shape.html

    def contours_find_by_channels(self, img):
        img = cv2.GaussianBlur(img, (5, 5), 0)
        all_contours = []
        for channel in cv2.split(img):
            for thrs in xrange(0, 255, 26):
                if thrs == 0:
                    bin = cv2.Canny(channel, 0, 50, apertureSize=5)
                    bin = cv2.dilate(bin, None)
                else:
                    retval, bin = cv2.threshold(channel, thrs, 255, cv2.THRESH_BINARY)
                bin, contours, hierarchy = cv2.findContours(bin, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
                all_contours += contours
        return all_contours

    def contours_find(self, bgImgDraw, channel, operation='contourFind'):
        contours = cv2.findContours(channel, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[1]
        self.log("contours.size found:", len(contours) )
        self.contours_draw(bgImgDraw, contours, operation=operation)
        return contours

    # contour get features

    # arc length
    # https://docs.opencv.org/3.1.0/dd/d49/tutorial_py_contour_features.html
    def contour_feature_isConvex(self, contour):
        return cv2.isContourConvex(contour)

    def contours_feature_perimeter(self, contours):
        contours_length = [(int(cv2.arcLength(contour, True)), contour) for contour in contours]
        return sorted(contours_length, key=lambda x: x[0], reverse=True)

    def contours_feature_area(self, contours):
        contour_sizes = [(int(cv2.contourArea(contour)), contour) for contour in contours]
        # areas = []
        # for c in contours:
        #     areas.append(cv2.contourArea(c))
        #     # Sort array of areas by size
        # contour_sizes = zip(areas, contours)
        # return contour_sizes
        return sorted(contour_sizes, key=lambda x: x[0], reverse=True)

    # It approximates a contour shape to another shape with less number of vertices depending upon the precision we specify.
    # It is an implementation of Douglas-Peucker algorithm.

    def contour_feature_boundingRect(self, contour):
        x,y,w,h = cv2.boundingRect(contour)
        #cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
        return x,y,w,h

    def contour_feature_minAreaRect(self, contour):
        rotatedRect = cv2.minAreaRect(contour)
        # https://docs.opencv.org/3.1.0/db/dd6/classcv_1_1RotatedRect.html
        # rotatedRect = (x, y), (width, height), rect_angle
        #  (x,y) the center point
        box = cv2.boxPoints(rotatedRect)
        box = np.int0(box)
        #cv2.drawContours(img,[box],0,(0,0,255),2)
        return box

    def contour_feature_minEnclosingTriangle(self, contour):
        retval, triangle = cv2.minEnclosingTriangle(contour)
        # https://docs.opencv.org/3.0-beta/modules/imgproc/doc/structural_analysis_and_shape_descriptors.html
        return retval, triangle

    def contour_feature_approxPoly(self, contour, epsilon_rate=0.1):
        epsilon = epsilon_rate*cv2.arcLength(contour,True)
        # second argument is called epsilon, which is maximum distance from contour to approximated contour.
        #       It is an accuracy parameter. A wise selection of epsilon is needed to get the correct output.
        # Third argument specifies whether curve is closed or not.
        approx = cv2.approxPolyDP(contour,epsilon,True)

    def contour_feature_convexHull(self, contour):
        hull = cv2.convexHull(contour)
        return hull

    def contour_feature_convexHull_pointIndexes(self, contour):
        pointIndexes = cv2.convexHull(contour, returnPoints=False)
        return pointIndexes

    def contour_feature_minEnclosingCircle(self,contour):
        (x,y),radius = cv2.minEnclosingCircle(contour)
        center = (int(x),int(y))
        radius = int(radius)
        # cv2.circle(img,center,radius,(0,255,0),2)
        return center,radius

    def contour_feature_fitEllipse(self, contour):
        ellipse = cv2.fitEllipse(contour)
        #cv2.ellipse(img,ellipse,(0,255,0),2)
        return ellipse

    def contour_feature_fitLine(self, img, contour):
        [vx,vy,x,y] = cv2.fitLine(contour, cv2.DIST_L2,0,0.01,0.01)
        self.draw_line(img, vx, vy, x, y)
        return vx, vy, x, y

    def draw_point(self, img, point, r):
        print 'point: '+ str(point)
        cv2.circle(img,point, r, 128, -1)

    def contour_feature_fitLine_2point(self, img, contour):
        [vx,vy,x,y] = cv2.fitLine(contour, cv2.DIST_L2,0,0.01,0.01)
        rows,cols = img.shape[:2]
        lefty = int((-x*vy/vx) + y)
        righty = int(((cols-x)*vy/vx)+y)
        point1 = (0,lefty)
        point2 = (cols-1,righty)
        # cv2.line(img,point1,point2,255,2)
        return point1,point2, vy/vx

    def GetLinePara(self, p1, p2):
        a = p1[1] - p2[1];
        b = p2[0] - p1[0];
        c = p1[0] *p2[1] - p2[0] * p1[1];
        return (a, b, c)

    # http://www.voidcn.com/article/p-gocejsxt-bqh.html
    # https://blog.csdn.net/abcjennifer/article/details/7584628
    def GetCrossPoint(self, point1, point2, point3, point4):
        a1,b1,c1 = self.GetLinePara(point1, point2);
        a2,b2,c2 = self.GetLinePara(point3, point4);

        d = a1 * b2 - a2 * b2
        x = (b1 * c2 - b2 * c1)*1.0 / d
        y = (c1 * a2 - c2 * a1)*1.0 / d
        return (int(x), int(y));



    def draw_line(self, img, vx, vy, x, y):
        rows,cols = img.shape[:2]
        lefty = int((-x*vy/vx) + y)
        righty = int(((cols-x)*vy/vx)+y)
        point1 = (0,lefty)
        point2 = (cols-1,righty)
        # cv2.line(img,point1,point2,(0,255,0),2)
        cv2.line(img,point1,point2,255,2)
        return point1,point2

    # contour filter
    def contours_filter_size(self, img, contours, min_width=50, min_height=50, min_area=None, operation='contourFilter'):
        self.log("contours.size before filter:", len(contours) )
        big_contours = list()
        for contour in contours:
            x, y, width, height = cv2.boundingRect(contour)
            area = cv2.contourArea(contour)
            if min_area is not None and area<min_area:
                continue
            if min_width is not None and width < min_width:
                continue
            if min_height is not None and height < min_height:
                continue

            big_contours.append(contour)
        self.log("contours.size after filter:", len(big_contours) )
        self.contours_draw(img, big_contours, operation=operation)
        return big_contours

    def contours_filter_area_nth_biggest(self, contours_area, n, operation='nthLargest'):
        sorted_areas = sorted(contours_area, key=lambda x: x[0], reverse=True)
        if sorted_areas and len(sorted_areas) >= n:
            # Find nth largest using data[n-1][1]
            return sorted_areas[n - 1][1]
        else:
            return None
        
    def contours_filter_area_biggest(self, contours_area):
        biggest_contour = max(contours_area, key=lambda x: x[0])[1]
        contours = self.contour_to_contours(biggest_contour)
        return contours

    # https://docs.opencv.org/3.1.0/dd/d49/tutorial_py_contour_features.html
    def contours_filter_triangles(self, contours):
        triangles = []
        for contour in contours:
            approx = cv2.approxPolyDP(contour,0.01*cv2.arcLength(contour,True),True)
            if len(approx) == 3:
                #triangle found
                triangles.append(contour)
        return triangles

    def contours_filter_squares(self, contours, cos_limit = 0.1):
        print('search for squares with threshold %f' % cos_limit)
        squares = []
        for cnt in contours:
            # https://docs.opencv.org/3.1.0/dd/d49/tutorial_py_contour_features.html
            cnt_len = cv2.arcLength(cnt, True)
            cnt = cv2.approxPolyDP(cnt, 0.02*cnt_len, True)
            if len(cnt) == 4 and cv2.contourArea(cnt) > 1000 and cv2.isContourConvex(cnt):
                cnt = cnt.reshape(-1, 2)
                max_cos = np.max([self.angle_cos( cnt[i], cnt[(i+1) % 4], cnt[(i+2) % 4] ) for i in xrange(4)])
                if max_cos < cos_limit :
                    squares.append(cnt)
                else:
                    #print('dropped a square with max_cos %f' % max_cos)
                    pass
        return squares

    def angle_cos(self, p0, p1, p2):
        d1, d2 = (p0-p1).astype('float'), (p2-p1).astype('float')
        return abs( np.dot(d1, d2) / np.sqrt( np.dot(d1, d1)*np.dot(d2, d2) ) )

    def contour_filter_minAreaRect(self, rectangle):
        (x, y), (width, height), rect_angle = rectangle # (x,y) the center point

        # Calculate angle and discard rects that has been rotated more than 15 degrees
        angle = 90 - rect_angle if (width < height) else -rect_angle
        if 15 < abs(angle) < 165:  # 180 degrees is maximum
            return False

        # We make basic validations about the regions detected based on its area and aspect ratio.
        # We only consider that a region can be a plate if the aspect ratio is approximately 520/110 = 4.727272
        # (plate width divided by plate height) with an error margin of 40 percent
        # and an area based on a minimum of 15 pixels and maximum of 125 pixels for the height of the plate.
        # These values are calculated depending on the image sizes and camera position:
        area = height * width

        if height == 0 or width == 0:
            return False
        if not self.satisfy_ratio(area, width, height):
            return False

        return True

    def satisfy_ratio(self, area, width, height):
        ratio = float(width) / float(height)
        if ratio < 1:
            ratio = 1 / ratio

        error = 0.4
        aspect = 4.7272  # In Estonia, car plate size: 52x11 aspect 4,7272
        # Plate number ssizes: https://en.wikipedia.org/wiki/Vehicle_registration_plate#Sizes
        # Set a min and max area. All other patches are discarded
        min = 15*aspect*15  # minimum area
        max = 125*aspect*125  # maximum area
        # Get only patches that match to a respect ratio.
        rmin = aspect - aspect*error
        rmax = aspect + aspect*error

        if (area < min or area > max) or (ratio < rmin or ratio > rmax):
            return False
        return True

    def contour_to_contours(self, contour):
        contours = [contour]
        return contours

    # contourIdx	Parameter indicating a contour to draw. If it is negative, all the contours are drawn.
    # https://docs.opencv.org/3.1.0/d6/d6e/group__imgproc__draw.html#ga746c0625f1781f1ffc9056259103edbc
    def contours_to_mask(self, im, contours, contourIdx=-1, saveRequired=False, operation='contour2Mask'):
        selection = np.zeros(im.shape[:2], np.uint8)
        cv2.drawContours(selection, contours,contourIdx, color=(255,255,255), thickness=-1)  # highlight contours
        # cv2.drawContours(selection, contours, -1, color=(0,255,0), thickness=-1)  # highlight contours
        # cv2.drawContours(selection, contours, -1, color=(0,0,255), thickness=1)
        return self.save(operation, selection, saveRequired)

    # https://docs.opencv.org/3.1.0/dd/d49/tutorial_py_contour_features.html
    def contours_to_ConvexPoly(self, img, contours):
        x, y = img.shape
        arr = np.zeros((x, y, 3), np.uint8)
        for i in range(len(contours)):
            cnt = contours[i]
            # https://docs.opencv.org/2.4/modules/core/doc/drawing_functions.html
            # Fills a convex polygon.
            cv2.fillConvexPoly(arr, cnt, [255, 255, 255])
        return arr

    def contours_draw(self, im, contours, contourIdx=-1, saveRequired=False, operation='contourDraw'):
        selection = im.copy()
        cv2.drawContours(selection, contours, contourIdx=contourIdx, color=(0, 255, 0), thickness=-1)  # highlight contours
        selection = cv2.addWeighted(im, 0.75, selection, 0.25, 0)  # outline contours
        cv2.drawContours(selection, contours, contourIdx=contourIdx, color=(0, 0, 255), thickness=1)
        return self.save(operation, selection, saveRequired)

    def edge_add(self, channel, edge_value=255, operation='edgeAdd'):
        h, w = self.get_shape(channel)
        channel_with_edge = np.full((h + 2, w + 2), edge_value, dtype=np.uint8)
        for i in range(0, h):
            for j in range(0, w):
                channel_with_edge[i + 1, j + 1] = channel[i, j]
        # self.savetxt(channel_with_edge)
        return self.save(operation, channel_with_edge)

    def edge_replace(self, channel, edge_value=255, operation='edgeReplace'):
        h, w = self.get_shape(channel)
        channel[0,0:(w-1)]=edge_value
        channel[(h-1),0:(w-1)]=edge_value
        channel[0:(h-1),0]=edge_value
        channel[0:(h-1),(w-1)]=edge_value
        # self.savetxt(channel)
        return self.save(operation, channel)

    def is_find_lane_failed(self, mask, y):
        h,w=self.get_shape(mask)
        failed_case_1 = (mask[y + 3, 3] == 255 and mask[y + 3, w - 3] == 255)
        failed_case_2 = (mask[h - 3, 3] == 0   and mask[h - 3, w - 3] == 0)

        return failed_case_1 or failed_case_2

    # https://blog.csdn.net/hust_bochu_xuchao/article/details/54019994
    # void unevenLightCompensate(Mat &image, int blockSize)
    # {
    #     if (image.channels() == 3) cvtColor(image, image, 7);
    #     double average = mean(image)[0];
    #     int rows_new = ceil(double(image.rows) / double(blockSize));
    #     int cols_new = ceil(double(image.cols) / double(blockSize));
    #     Mat blockImage;
    #     blockImage = Mat::zeros(rows_new, cols_new, CV_32FC1);
    #     for (int i = 0; i < rows_new; i++)
    #     {
    #     for (int j = 0; j < cols_new; j++)
    #     {
    #         int rowmin = i*blockSize;
    #     int rowmax = (i + 1)*blockSize;
    #     if (rowmax > image.rows) rowmax = image.rows;
    #     int colmin = j*blockSize;
    #     int colmax = (j + 1)*blockSize;
    #     if (colmax > image.cols) colmax = image.cols;
    #     Mat imageROI = image(Range(rowmin, rowmax), Range(colmin, colmax));
    #     double temaver = mean(imageROI)[0];
    #     blockImage.at<float>(i, j) = temaver;
    #     }
    #     }
    #     blockImage = blockImage - average;
    #     Mat blockImage2;
    #     resize(blockImage, blockImage2, image.size(), (0, 0), (0, 0), INTER_CUBIC);
    #     Mat image2;
    #     image.convertTo(image2, CV_32FC1);
    #     Mat dst = image2 - blockImage2;
    #     dst.convertTo(image, CV_8UC1);
    # }

    # def light_balance(self, image, blockSize=32, operation='light'):


    # def draw_selection_flood(self, im, mask, operation='result'):
    #     selection = im.copy()
    #
    #     mask.convertTo(mask, cv2.CV_32SC1); # ?
    #     contours = cv2.findContours(mask, cv2.RETR_FLOODFILL, cv2.CHAIN_APPROX_SIMPLE)[1]
    #     self.log("contours.size:", len(contours) )
    #
    #     # for i in range(0, len(contours)):
    #     #     contour = contours[i]
    #     #     contours.erase(contour)
    #
    #     cv2.drawContours(selection, contours, -1, color=(0,255,0), thickness=-1)  # highlight contours
    #
    #     selection = cv2.addWeighted(im, 0.75, selection, 0.25, 0)  # outline contours
    #     cv2.drawContours(selection, contours, -1, color=(0,0,255), thickness=1)
    #     return self.save(operation, selection)

    ### solutions
    def process_image_v1(self, image, saveRequired=False):
        self.set_version('v1')
        h,w=self.get_shape(image)
        mask=self.flood_fill(image, (3,)*3, (20,)*3, (w/2, h/2))

    def process_image_v2(self, image, saveRequired=False):
        self.set_version('v2')
        h,w=self.get_shape(image)
        mask=self.flood_fill(image, (3,)*3, (20,)*3, (w/2, h/2))

        self.unique("mask", mask)
        self.savetxt(mask)

        threshold=self.threshold(mask, 90, 255)
        self.unique("threshold", threshold)
        self.savetxt(threshold)

    def process_image_v3_1(self, image, saveRequired=False):
        self.set_version('v3.1')
        mask=self.flood_fill(image, (3,)*3, (20,)*3)
        mask_filled=self.fill_holes(mask)
        return self.contours_draw(image, mask_filled, saveRequired)

    def process_image_v3_2(self, image, saveRequired=False):
        self.set_version('v3.2')
        gray=self.rgb2gray(image)
        blur=self.blur_gaussian(gray, 5)
        mask=self.flood_fill_fixed(blur,(43,)*3, (43,)*3)
        mask_filled=self.fill_holes(mask)
        return self.contours_draw(image, mask_filled, saveRequired)

    def process_image_v3_3(self, image, saveRequired=False):
        self.set_version('v3.3')
        gray=self.rgb2gray(image)
        threshold=self.threshold_adaptive(gray, 29, 1)
        morph=self.morph_close(threshold, (5,4))
        morph=self.morph_open(morph, (3,3))
        mask=self.flood_fill_fixed(morph, (70,)*3, (70,)*3)
        mask_filled=self.fill_holes(mask)
        return self.contours_draw(image, mask_filled, saveRequired)



    def process_image_v3_3_1(self, image, saveRequired=False):
        self.set_version('v3.3.1')
        image = self.imread("../20181215/image8_text.jpg")
        gray=self.rgb2gray(image)
        threshold=self.threshold_adaptive(gray, 11, 5, adaptiveMethod=cv2.ADAPTIVE_THRESH_MEAN_C)
        threshold=self.threshold_adaptive(gray, 11, 5, adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C)

    def process_image_v3_5(self, image, saveRequired=False):
        self.set_version('v3.5')
        image = self.imread("../20181215/image9_cow.jpg")
        self.sharpen_laplace(image)

    def process_image_v3_7(self, image, saveRequired=False):
        self.set_version('v3.7')
        image = self.imread("../20181215/image4_shadow.png")
        self.sharpen_gamma(image, gamma=0.2)

    def process_image_v3_6(self, image, saveRequired=False):
        self.set_version('v3.6')
        image = self.rgb2hsv(image)
        h,s,v = self.split(image)
        self.save('h', h)
        self.save('s', s)
        self.save('v', v)


        # morph=self.morph_close(threshold, (5,4))
        # # morph=self.morph_open(morph, (3,3))
        # mask=self.flood_fill_fixed(morph, (70,)*3, (70,)*3)
        # mask_filled=self.fill_holes(mask)
        # return self.contours_draw(image, mask_filled, saveRequired)

    def process_image_v4_0(self, image, saveRequired=False):
        self.set_version('v4.0')
        gray=self.rgb2gray(image)
        self.hist(gray)
        threshold=self.threshold(gray, 195)
        morph=self.morph_close(threshold, (7,7)) # 6. connect cutted line
        mask=self.flood_fill_fixed(morph, (1,)*3, (1,)*3)
        return self.contours_draw(image, mask, saveRequired)

    def process_image_v4_1(self, image, saveRequired=False):
        self.set_version('v4.1')
        gray=self.rgb2gray(image)
        self.hist(gray)
        threshold=self.threshold(gray, 195)
        morph=self.morph_close(threshold, (7,7)) # 6. connect cutted line
        mask=self.flood_fill_fixed(morph, (1,)*3, (1,)*3)
        return self.contours_draw(image, mask, saveRequired)

    def process_image_v4_2(self, image, saveRequired=False):
        self.set_version('v4.2')
        gray=self.rgb2gray(image)
        # self.draw_selection(image, gray)

        # canny_edges = self.canny(gray, 50, 150) # low_threshold, high_threshold
        # canny_edges = self.canny(gray, 90, 255)
        canny_edges = self.canny(gray, 100, 255)


        contours = cv2.findContours(gray, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[1]
        self.log("contours.size:", len(contours) )

        h,w=self.get_shape(image)
        canny_edges[0:h/5,0:w]=255

        mask=self.flood_fill_fixed(canny_edges, (1,)*3, (1,)*3)
        mask_filled=self.fill_holes(mask)
        return self.draw_selection(image, mask_filled, saveRequired)




    def process_image_v4_3(self, image, saveRequired=False):
        self.set_version('v4.3')
        gray=self.rgb2gray(image)



        # canny_edges = self.canny(gray, 50, 150) # low_threshold, high_threshold
        # canny_edges = self.canny(gray, 90, 255)
        # canny_edges = self.canny(gray, 100, 255)
        canny_edges = self.canny(gray, 40, 60, apertureSize=3,L2gradient=True)

        contours = self.contours_find(image, canny_edges)
        contours = self.contours_filter_size(image, contours, 50, 50)


        contours = self.contours_to_mask(image, contours, True)
        # dilate = self.morph_dilate(contours, (10,10), 1)
        dilate = self.morph_close(contours, (10, 10))
        #  Denoising
        # cleaned = contours.copy()
        # cleaned = cv2.fastNlMeansDenoising(contours, cleaned, 70)
        # # cleaned = cv2.fastNlMeansDenoisingColored(canny_edges,None,10,10,7,21)
        # self.save('clean', cleaned, True)


        h,w=self.get_shape(image)
        # canny_edges[0:h/5,0:w]=255
        dilate[h-5:h,0:w]=0

        # mask1=self.flood_fill_fixed(contours, (1,)*3, (1,)*3, (w/2-25, h-25))
        mask=self.flood_fill_fixed(dilate, (1,)*3, (1,)*3, (w/2, h-3))
        # mask3=self.flood_fill_fixed(contours, (1,)*3, (1,)*3, (w/2+33, h-35))
        # mask = self.bitwise_or(mask1, mask2)
        # mask = self.bitwise_or(mask, mask3)
        mask_filled=self.fill_holes(mask)
        # mask_filled = self.morph_close(mask, (10, 10))

        contours = self.contours_find(image, mask_filled)
        return self.contours_draw(image, contours, saveRequired)

    def process_image_v4_4(self, image, saveRequired=False):
        self.set_version('v4.4')
        gray_image = self.rgb2gray(image)
        # self.savetxt(gray_image)
        # self.hist(gray_image)
        img_hsv = self.rgb2hsv(image)
        mask_yw = self.mask_to_yellow_or_white(gray_image, img_hsv)
        mask_yw_image = self.bitwise_and(gray_image, mask_yw)

        gauss_gray = self.blur_gaussian(mask_yw_image, 5) # kernel_size

        canny_edges = self.canny(gauss_gray, 50, 150) # low_threshold, high_threshold
        # canny_edges = self.canny(gray, 50, 150) # low_threshold, high_threshold
        # canny_edges = self.canny(gray, 90, 255)
        # canny_edges = self.canny(gray, 100, 255)
        canny_edges = self.canny(gray_image, 40, 60, apertureSize=3,L2gradient=True)

        contours = self.contours_find(image, canny_edges)
        contours = self.contours_filter_size(image, contours, 50, 50)


        contours = self.contours_to_mask(image, contours, True)
        # dilate = self.morph_dilate(contours, (10,10), 1)
        dilate = self.morph_close(contours, (10, 10))
        #  Denoising
        # cleaned = contours.copy()
        # cleaned = cv2.fastNlMeansDenoising(contours, cleaned, 70)
        # # cleaned = cv2.fastNlMeansDenoisingColored(canny_edges,None,10,10,7,21)
        # self.save('clean', cleaned, True)


        h,w=self.get_shape(image)
        # canny_edges[0:h/5,0:w]=255
        dilate[h-5:h,0:w]=0

        # mask1=self.flood_fill_fixed(contours, (1,)*3, (1,)*3, (w/2-25, h-25))
        mask=self.flood_fill_fixed(dilate, (1,)*3, (1,)*3, (w/2, h-3))
        # mask3=self.flood_fill_fixed(contours, (1,)*3, (1,)*3, (w/2+33, h-35))
        # mask = self.bitwise_or(mask1, mask2)
        # mask = self.bitwise_or(mask, mask3)
        mask_filled=self.fill_holes(mask)
        # mask_filled = self.morph_close(mask, (10, 10))

        contours = self.contours_find(image, mask_filled)
        return self.contours_draw(image, contours, saveRequired)

    def process_image_v5(self, image, saveRequired=False):
        self.set_version('v5')
        gray_image = self.rgb2gray(image)
        # self.savetxt(gray_image)
        # self.hist(gray_image)
        img_hsv = self.rgb2hsv(image)
        mask_yw = self.mask_to_yellow_or_white(gray_image, img_hsv)
        mask_yw_image = self.bitwise_and(gray_image, mask_yw)

        gauss_gray = self.blur_gaussian(mask_yw_image, 5) # kernel_size
        canny_edges = self.canny(gauss_gray, 50, 150) # low_threshold, high_threshold

        h,w=self.get_shape(image)
        lower_left = [w/9, h]
        lower_right = [w - w/9, h]
        top_left = [w/2 - w/8, h/2 + h/10]
        top_right = [w/2 + w/8, h/2 + h/10]
        vertices = [np.array([
            lower_left,
            top_left,
            top_right,
            lower_right], dtype=np.int32)]
        roi_image = self.region_of_interest(canny_edges, vertices)

        # rho and theta are the distance and angular resolution of the grid in Hough space, same values as quiz
        rho = 2
        theta = np.pi/180
        # threshold is minimum number of intersections in a grid for candidate line to go to output
        threshold = 20
        min_line_len = 50
        max_line_gap = 200

        line_image = self.hough_lines(roi_image, rho, theta, threshold, min_line_len, max_line_gap)
        result = self.weighted_img(line_image, image, a=0.8, b=1., r=0.)
        return self.save('result', result, saveRequired)

    def example(self):
        img = np.array([
            [1, 0, 1, 1, 2],
            [1, 0, 1, 1, 2],
            [1, 0, 1, 1, 2],
            [1, 0, 1, 1, 2],
            [1, 0, 1, 1, 2]], dtype=np.uint8)
        print "img:" + str(img)
        new_img = np.zeros_like(img)  # step 1
        print "new_img:" + str(new_img)
        print "np.unique(img):" + str(np.unique(img))
        for val in np.unique(img)[1:]:  # step 2
            print "val:" + str(val)
            mask = np.uint8(img == val)  # step 3
            print "mask:" + str(mask)
            labels, stats = cv2.connectedComponentsWithStats(mask, 4)[1:3]  # step 4
            print "labels:" + str(labels)
            print "stats:" + str(stats)
            print "stats[1:, cv2.CC_STAT_AREA]:" + str(stats[1:, cv2.CC_STAT_AREA])
            largest_label = 1 + np.argmax(stats[1:, cv2.CC_STAT_AREA])  # step 5
            print "largest_label:" + str(largest_label)
            new_img[labels == largest_label] = val  # step 6
            print "new_img:" + str(new_img)
        print(new_img)

    def process_video_v5(self, file_name, saveRequired=False):
        it.debug=False
        cap = cv2.VideoCapture(file_name)

        # Check if camera opened successfully
        if (cap.isOpened()== False):
            print("Error opening video stream or file")

        # Default resolutions of the frame are obtained.The default resolutions are system dependent.
        # We convert the resolutions from float to integer.
        frame_width = int(cap.get(3))
        frame_height = int(cap.get(4))
        out = cv2.VideoWriter('outpy.mp4', 0x00000021, 15.0, (frame_width,frame_height))

        # Read until video is completed
        while(cap.isOpened()):
            # Capture frame-by-frame
            ret, frame = cap.read()
            if ret == True:
                processed = self.process_image_v4_2(frame)

                cv2.imshow('frame',processed)
                out.write(processed)

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

            # Break the loop
            else:
                break

        cap.release()
        out.release()
        cv2.destroyAllWindows()

    def process_image_v6_0(self, image, saveRequired=False):
        self.set_version('v6.0')
        gray=self.rgb2gray(image)


        adaptive=self.threshold_adaptive(gray, 29, 1)

        integral_full=self.threshold_integral(gray, sub_thresh=0.01)
        integral_thin=self.threshold_integral(gray, sub_thresh=-0.15)
        integral_thin_close = self.morph_close(integral_thin, kernel_size=(5,5))

        gauss_gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gauss_gray, 50, 110) # low_threshold, high_threshold
        canny = self.morph_close(canny, kernel_size=(10,10))

        # c_if = self.bitwise_and(integral_full, canny)
        # c_a = self.bitwise_and(adaptive, canny)
        # c_a_if = self.bitwise_and(c_a, integral_full)
        #
        # c_it = self.bitwise_or(integral_thin_close, canny)
        # c_a_if_c = self.bitwise_or(c_a_if, canny)
        # c_a_if_it = self.bitwise_or(integral_thin_close, c_a_if)

        #
        # # self.save('c_if', c_if, saveRequired=True)
        # c_if = self.morph_close(c_if, kernel_size=(3,3))
        # mask=self.flood_fill_fixed(c_if, (1,)*3, (1,)*3)
        # contours = self.contours_find(image, mask)
        # c_if = self.contours_draw(image, contours, saveRequired)
        #
        # # self.save('c_a', c_a, saveRequired=True)
        # c_a = self.morph_close(c_a, kernel_size=(3,3))
        # mask=self.flood_fill_fixed(c_a, (1,)*3, (1,)*3)
        # contours = self.contours_find(image, mask)
        # c_a = self.contours_draw(image, contours, saveRequired)
        #
        # # self.save('c_a_if', c_a_if, saveRequired=True)
        # c_a_if = self.morph_close(c_a_if, kernel_size=(3,3))
        # mask=self.flood_fill_fixed(c_a_if, (1,)*3, (1,)*3)
        # contours = self.contours_find(image, mask)
        # c_a_if = self.contours_draw(image, contours, saveRequired)
        #
        # # self.save('c_it', c_it, saveRequired=True)
        # mask=self.flood_fill_fixed(c_it, (1,)*3, (1,)*3)
        # contours = self.contours_find(image, mask)
        # c_it = self.contours_draw(image, contours, saveRequired)
        #
        # # self.save('c_a_if_c', c_a_if_c, saveRequired=True)
        # mask=self.flood_fill_fixed(c_a_if_c, (1,)*3, (1,)*3)
        # contours = self.contours_find(image, mask)
        # c_a_if_c = self.contours_draw(image, contours, saveRequired)
        #
        # # self.save('c_a_if_it', c_a_if_it, saveRequired=True)
        # c_a_if_it = self.morph_close(c_a_if_it, kernel_size=(3,3))
        # mask=self.flood_fill_fixed(c_a_if_it, (1,)*3, (1,)*3)
        # contours = self.contours_find(image, mask)
        # c_a_if_it = self.contours_draw(image, contours, saveRequired)

        ###################################################################################################################


        # mask_filled=self.fill_holes(mask)
        # self.save('c_if_mask_filled', mask_filled, saveRequired=True)

        # contours = self.contours_find(image, canny_edges)
        # contours = self.contours_to_mask(image, contours, True)

        # ai = self.bitwise_and(adaptive, integral_full)
        # self.bitwise_or(cc, integral_thin_close)


        # canny_edges = self.canny(gray, 50, 150) # low_threshold, high_threshold
        # canny_edges = self.canny(gray, 90, 255)

        # canny_edges = self.canny(gauss_gray, 30, 60) # low_threshold, high_threshold
        # contours = self.contours_filter(image, contours, 50, 50)

        # gray = self.sharpen_laplace(gray)
        # canny_edges = self.canny(gray, 120, 255)


        # contours = self.contours_find(image, gray)
        # contours = self.contours_filter(image, contours, 50, 50)
        # contours = self.contours_draw_bw(image, contours, True)


        # threshold=self.threshold_adaptive(gray, 29, 7)
        # threshold=self.threshold_adaptive(gray, 91, 1)
        # threshold=self.threshold_adaptive(gray, 91, 7)
        # threshold=self.threshold_adaptive(gray, 3, 1)

        # morph=self.morph_close(threshold, (5,4))
        # morph=self.morph_open(morph, (3,3))
        # mask=self.flood_fill_fixed(morph, (70,)*3, (70,)*3)
        # mask_filled=self.fill_holes(mask)
        # return self.contours_draw(image, mask_filled, saveRequired)

        # canny_edges = self.canny(gray, 100, 255)
        # canny_edges = self.canny(gray, 40, 60, apertureSize=3,L2gradient=True)

        # hsv = self.rgb2hsv(image)
        # h,s,v = self.split(hsv)
        # s =self.sharpen_gamma(s, gamma=0.3)
        # v[0:len(v)]=128
        # hsv = self.merge((h, s, v))
        # image = self.hsv2rgb(hsv)

        # image2 =self.sharpen_gamma(image, gamma=3)
        # image3 =self.sharpen_gamma(image, gamma=5)
        # image = self.sharpen_laplace(image)
        # image = self.blur_gaussian(image, 5)


        # h,w = self.get_shape(image)
        # S = max(h, w)/8
        # s2 = S/2
        # threshold=self.threshold_integral(gray_image, sub_thresh=-0.15, win_length=s2)


        # morph=self.morph_close(threshold, (5,4))
        # # morph=self.morph_open(morph, (3,3))
        # mask=self.flood_fill_fixed(morph, (70,)*3, (70,)*3)
        # mask_filled=self.fill_holes(mask)
        # return self.contours_draw(image, mask_filled, saveRequired)



    def process_image_v6_1(self, image, saveRequired=False):
        self.set_version('v6.1')
        gray=self.rgb2gray(image)

        integral_full=self.threshold_integral(gray, sub_thresh=0.01)

        gauss_gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gauss_gray, 50, 110) # low_threshold, high_threshold
        canny = self.morph_close(canny, kernel_size=(10,10))

        c_if = self.bitwise_and(integral_full, canny)

        # self.save('c_if', c_if, saveRequired=True)
        c_if = self.morph_close(c_if, kernel_size=(3,3))
        mask=self.flood_fill_fixed(c_if, (1,)*3, (1,)*3)
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)


    def process_image_v6_2(self, image, saveRequired=False):
        self.set_version('v6.2')
        gray=self.rgb2gray(image)


        adaptive=self.threshold_adaptive(gray, 29, 1)

        gauss_gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gauss_gray, 50, 110) # low_threshold, high_threshold
        canny = self.morph_close(canny, kernel_size=(10,10))

        c_a = self.bitwise_and(adaptive, canny)

        # self.save('c_a', c_a, saveRequired=True)
        c_a = self.morph_close(c_a, kernel_size=(3,3))
        h,w = self.get_shape(image)
        c_a[0:h/5,0:w]=255
        mask=self.flood_fill_fixed(c_a, (1,)*3, (1,)*3)
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)

    def process_image_v6_3(self, image, saveRequired=False):
        self.set_version('v6.3')
        gray=self.rgb2gray(image)


        adaptive=self.threshold_adaptive(gray, 29, 1)

        integral_full=self.threshold_integral(gray, sub_thresh=0.01)

        gauss_gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gauss_gray, 50, 110) # low_threshold, high_threshold
        canny = self.morph_close(canny, kernel_size=(10,10))

        c_a = self.bitwise_and(adaptive, canny)
        c_a_if = self.bitwise_and(c_a, integral_full)


        # self.save('c_a_if', c_a_if, saveRequired=True)
        c_a_if = self.morph_close(c_a_if, kernel_size=(3,3))
        mask=self.flood_fill_fixed(c_a_if, (1,)*3, (1,)*3)
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)


    def process_image_v6_4(self, image, saveRequired=False):
        self.set_version('v6.4')
        gray=self.rgb2gray(image)

        integral_thin=self.threshold_integral(gray, sub_thresh=-0.15)
        integral_thin_close = self.morph_close(integral_thin, kernel_size=(5,5))

        gauss_gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gauss_gray, 50, 110) # low_threshold, high_threshold
        canny = self.morph_close(canny, kernel_size=(10,10))

        c_it = self.bitwise_or(integral_thin_close, canny)
        h,w = self.get_shape(image)
        c_it[0:h/5,0:w]=255
        # self.save('c_it', c_it, saveRequired=True)
        mask=self.flood_fill_fixed(c_it, (1,)*3, (1,)*3)
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)

    def process_image_v6_5(self, image, saveRequired=False):
        self.set_version('v6.5')
        gray=self.rgb2gray(image)


        adaptive=self.threshold_adaptive(gray, 29, 1)

        integral_full=self.threshold_integral(gray, sub_thresh=0.01)

        gauss_gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gauss_gray, 50, 110) # low_threshold, high_threshold
        canny = self.morph_close(canny, kernel_size=(10,10))

        c_a = self.bitwise_and(adaptive, canny)
        c_a_if = self.bitwise_and(c_a, integral_full)

        c_a_if_c = self.bitwise_or(c_a_if, canny)

        # self.save('c_a_if_c', c_a_if_c, saveRequired=True)
        mask=self.flood_fill_fixed(c_a_if_c, (1,)*3, (1,)*3)
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)


    def process_image_v6_6(self, image, saveRequired=False):
        self.set_version('v6.6')
        gray=self.rgb2gray(image)


        adaptive=self.threshold_adaptive(gray, 29, 1)

        integral_full=self.threshold_integral(gray, sub_thresh=0.01)
        integral_thin=self.threshold_integral(gray, sub_thresh=-0.15)
        integral_thin_close = self.morph_close(integral_thin, kernel_size=(5,5))

        gauss_gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gauss_gray, 50, 110) # low_threshold, high_threshold
        canny = self.morph_close(canny, kernel_size=(10,10))

        c_a = self.bitwise_and(adaptive, canny)
        c_a_if = self.bitwise_and(c_a, integral_full)

        c_a_if_it = self.bitwise_or(integral_thin_close, c_a_if)

        # self.save('c_a_if_it', c_a_if_it, saveRequired=True)
        c_a_if_it = self.morph_close(c_a_if_it, kernel_size=(3,3))
        h,w = self.get_shape(image)
        c_a_if_it[0:h/5,0:w]=255
        mask=self.flood_fill_fixed(c_a_if_it, (1,)*3, (1,)*3)
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)

    def process_image_v6_2_1(self, image, saveRequired=False):
        self.set_version('v6.2.1')
        gray=self.rgb2gray(image)

        adaptive=self.threshold_adaptive(gray, 29, 1)

        gauss_gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gauss_gray, 50, 110) # low_threshold, high_threshold

        # canny = self.canny(gray, 100, 150)

        # adaptive = self.add_edge(adaptive, edge_value=0)
        # canny = self.add_edge(canny)
        # # canny = self.morph_close(canny, kernel_size=(15,15))

        canny = self.morph_dilate(canny, kernel_size=(7,7))


        c_a = self.bitwise_and(adaptive, canny)


        # self.save('c_a', c_a, saveRequired=True)
        c_a = self.morph_close(c_a, kernel_size=(3,3))
        h,w = self.get_shape(image)
        c_a[0:h/5,0:w]=255
        mask=self.flood_fill_fixed(c_a, (1,)*3, (1,)*3)
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)

    def process_image_v6_2_2(self, image, saveRequired=False):
        self.set_version('v6.2.2')
        gray=self.rgb2gray(image)

        adaptive=self.threshold_adaptive(gray, 29, 1)

        erode=self.morph_erode(adaptive, kernel_size=(3,3))
        erode = self.contours_find(image, erode)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)
        erode = self.morph_dilate(erode, kernel_size=(3,3))

        channel=erode
        h,w = self.get_shape(image)
        channel[0:h/5,0:w]=255
        mask=self.flood_fill_fixed(channel, (1,)*3, (1,)*3)
        mask=self.fill_holes(mask)
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)

        # open=self.morph_open(adaptive, kernel_size=(3,3))
        # open = self.contours_find(image, open)
        # open = self.contours_filter(image, open, 50, 50)
        # open = self.contours_to_mask(image, open, True)

        # gray = self.blur_gaussian(gray, 5) # kernel_size
        # canny = self.canny(gray, 50, 110) # low_threshold, high_threshold
        #
        # # canny = self.canny(gray, 100, 150)
        #
        # # adaptive = self.add_edge(adaptive, edge_value=0)
        # # canny = self.add_edge(canny)
        # # # canny = self.morph_close(canny, kernel_size=(15,15))
        #
        # canny = self.morph_dilate(canny, kernel_size=(7,7))
        #
        #
        # c_a = self.bitwise_and(adaptive, canny)


        # self.save('c_a', c_a, saveRequired=True)
        # channel = self.morph_close(erode, kernel_size=(3,3))

    def process_image_v6_2_3(self, image, saveRequired=False): # fix file_holes using edge_replace
        self.set_version('v6.2.3')
        gray=self.rgb2gray(image)

        adaptive=self.threshold_adaptive(gray, 29, 1)
        erode=self.morph_erode(adaptive, kernel_size=(3,3))

        erode = self.contours_find(image, erode)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)
        erode = self.morph_dilate(erode, kernel_size=(3,3))

        channel=erode
        h,w = self.get_shape(image)
        channel[0:h/5,0:w]=255
        mask=self.flood_fill_fixed(channel, (1,)*3, (1,)*3)
        mask=self.edge_replace(mask, edge_value=255)
        mask=self.fill_holes(mask)
        # mask=self.morph_close(mask, kernel_size=(13,13))
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)

    def process_image_v6_2_4(self, image, saveRequired=False): # replace thresholdAdaptive by canny, because of 622.png shake
        self.set_version('v6.2.4')
        gray=self.rgb2gray(image)
        canny = self.canny(gray, 60, 100) # low_threshold, high_threshold
        erode = self.contours_find(image, canny)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)
        # erode = self.morph_dilate(erode, kernel_size=(10,10))
        erode=self.morph_close(erode, kernel_size=(20,20))
        channel=erode
        h,w = self.get_shape(image)
        channel[0:h/5,0:w]=255 # zone not interest
        mask=self.flood_fill_fixed(channel, (1,)*3, (1,)*3)
        # mask=self.edge_replace(mask, edge_value=255)
        # mask=self.fill_holes(mask)
        mask=self.morph_close(mask, kernel_size=(13,13))
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)



        # adaptive=self.threshold_adaptive(gray, 29, 1)
        # erode=self.morph_erode(adaptive, kernel_size=(3,3))
        # erode = self.contours_find(image, erode)
        # erode = self.contours_filter(image, erode, 50, 50)
        # erode = self.contours_to_mask(image, erode)
        # erode = self.morph_dilate(erode, kernel_size=(3,3))
        #
        # channel=erode
        # h,w = self.get_shape(image)
        # channel[0:h/5,0:w]=255 # zone not interest
        # mask=self.flood_fill_fixed(channel, (1,)*3, (1,)*3)
        # mask=self.edge_replace(mask, edge_value=255)
        # mask=self.fill_holes(mask)
        # # mask=self.morph_close(mask, kernel_size=(13,13))
        # contours = self.contours_find(image, mask)
        # return self.contours_draw(image, contours, saveRequired)

        # gray=self.sharpen_laplace(gray)
        # canny = self.canny(gray, 30, 210) # low_threshold, high_threshold
        # canny = self.canny(gray, 100, 160) # low_threshold, high_threshold

    def process_image_v6_2_5(self, image, saveRequired=False): # replace morphclose by fileholes
        self.set_version('v6.2.5')
        gray=self.rgb2gray(image)
        canny = self.canny(gray, 60, 100) # low_threshold, high_threshold
        erode = self.contours_find(image, canny)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)
        # erode = self.morph_dilate(erode, kernel_size=(10,10))
        erode=self.morph_close(erode, kernel_size=(20,20))
        channel=erode
        h,w = self.get_shape(image)
        channel[0:h/5,0:w]=255 # zone not interest
        mask=self.flood_fill_fixed(channel, (1,)*3, (1,)*3)
        mask=self.edge_replace(mask, edge_value=255)
        mask=self.fill_holes(mask)
        # mask=self.morph_close(mask, kernel_size=(13,13))
        contours = self.contours_find(image, mask)
        return self.contours_draw(image, contours, saveRequired)

    def process_image_v6_2_7(self, image, saveRequired=False): # 625 is not good for 626.png
        self.set_version('v6.2.7')
        gray=self.rgb2gray(image)

        focus_mesure = self.blur_measure(gray)
        print 'focus_mesure: ' +   str(focus_mesure)

        if focus_mesure>200: # image626: 418
            gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gray, 60, 100) # low_threshold, high_threshold
        erode = self.contours_find(image, canny)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)
        # erode = self.morph_dilate(erode, kernel_size=(10,10))
        erode=self.morph_close(erode, kernel_size=(20,20))
        channel=erode
        h,w = self.get_shape(image)
        channel[0:h/5,0:w]=255 # zone not interest
        mask=self.flood_fill_fixed(channel, (1,)*3, (1,)*3)
        mask=self.edge_replace(mask, edge_value=255)
        mask=self.fill_holes(mask)
        contours = self.contours_find(image, mask)
        # return self.contours_to_mask(image, contours, saveRequired)
        return self.contours_draw(image, contours, saveRequired)

    def process_image_v6_2_8(self, image, saveRequired=False): # the preview version is not good for 627.png
        self.set_version('v6.2.8')
        h,w = self.get_shape(image)
        gray=self.rgb2gray(image)

        focus_mesure = self.blur_measure(gray)
        print 'focus_mesure: ' +   str(focus_mesure)

        if focus_mesure>200: # image626: 418
            gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gray, 60, 100) # low_threshold, high_threshold
        erode = self.contours_find(image, canny)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)


        erode[ (h-50):h, (w/4): (w/4)*3]=0 # zone not interest
        self.save('roi', erode)

        # erode = self.morph_dilate(erode, kernel_size=(10,10))
        erode=self.morph_close(erode, kernel_size=(20,20))
        channel=erode

        channel[0:h/5,0:w]=255 # zone not interest
        mask=self.flood_fill_fixed(channel, (1,)*3, (1,)*3)
        mask=self.edge_replace(mask, edge_value=255)
        mask=self.fill_holes(mask)
        contours = self.contours_find(image, mask)
        # return self.contours_to_mask(image, contours, saveRequired)
        return self.contours_draw(image, contours, saveRequired)

    def process_image_v6_2_9(self, image, saveRequired=False): # the preview version is not good for 628.png
        self.set_version('v6.2.9')
        h,w = self.get_shape(image)
        gray=self.rgb2gray(image)

        focus_mesure = self.blur_measure(gray)
        print 'focus_mesure: ' +   str(focus_mesure)

        if focus_mesure>200: # image626: 418
            gray = self.blur_gaussian(gray, 5) # kernel_size
        canny = self.canny(gray, 60, 100) # low_threshold, high_threshold


        erode = self.contours_find(image, canny)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)


        erode[ (h-50):h, (w/4): (w/4)*3]=0 # zone not interest
        self.save('roi', erode)
        # erode = self.morph_dilate(erode, kernel_size=(10,10))
        erode=self.morph_close(erode, kernel_size=(20,20))


        channel=erode

        channel[0:h/5,0:w]=255 # zone not interest
        mask=self.flood_fill_fixed(channel, (1,)*3, (1,)*3)

        # mask=self.edge_replace(mask, edge_value=255)
        # mask=self.fill_holes(mask)
        # mask=self.morph_close(mask, kernel_size=(13,13))

        contours = self.contours_find(image, mask)
        # return self.contours_to_mask(image, contours, saveRequired)
        return self.contours_draw(image, contours, saveRequired)


    def process_image_v6_2_9(self, image, saveRequired=False): # the preview version is not good for 628.png
        self.set_version('v6.2.9')
        h,w = self.get_shape(image)
        gray=self.rgb2gray(image)

        focus_mesure = self.blur_measure(gray)
        print 'focus_mesure: ' +   str(focus_mesure)

        if focus_mesure>200: # image626: 418
            gray = self.blur_gaussian(gray, 5) # kernel_size
        if focus_mesure<50:
            gray = self.sharpen_gamma(gray, 0.8)
        canny = self.canny(gray, 60, 100) # low_threshold, high_threshold
        erode = self.contours_find(image, canny)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)
        erode[ (h-50):h, (w/4): (w/4)*3]=0 # zone not interest
        self.save('roi', erode)
        # erode = self.morph_dilate(erode, kernel_size=(10,10))
        erode=self.morph_close(erode, kernel_size=(20,20))

        # adaptive=self.threshold_adaptive(gray, 29, 1)
        # erode=self.morph_erode(adaptive, kernel_size=(3,3))
        # erode = self.contours_find(image, erode)
        # erode = self.contours_filter(image, erode, 50, 50)
        # erode = self.contours_to_mask(image, erode)
        # erode = self.morph_dilate(erode, kernel_size=(3,3))

        channel=erode
        channel[0:h/5,0:w]=255 # zone not interest
        mask=self.flood_fill_fixed(channel, (1,)*3, (1,)*3)

        # mask=self.edge_replace(mask, edge_value=255)
        # mask=self.fill_holes(mask)
        # mask=self.morph_close(mask, kernel_size=(13,13))
        if not self.is_find_lane_failed(mask, h/5):
            contours = self.contours_find(image, mask)
            # return self.contours_to_mask(image, contours, saveRequired)
            return self.contours_draw(image, contours, saveRequired)
        else:
            return image

    def process_image_v6_2_10(self, image, saveRequired=False): # add fitline
        self.set_version('v6.2.10')
        h,w = self.get_shape(image)
        gray=self.rgb2gray(image)

        focus_mesure = self.blur_measure(gray)
        print 'focus_mesure: ' +   str(focus_mesure)

        if focus_mesure>200: # image626: 418
            gray = self.blur_gaussian(gray, 5) # kernel_size
        if focus_mesure<50:
            gray = self.sharpen_gamma(gray, 0.8)
        canny = self.canny(gray, 60, 100) # low_threshold, high_threshold
        erode = self.contours_find(image, canny)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)
        # erode[ (h-50):h, (w/4): (w/4)*3]=0 # zone not interest
        self.save('roi', erode)
        erode=self.morph_close(erode, kernel_size=(20,20))

        channel=erode
        channel[0:h/5,0:w]=255 # zone not interest
        mask=self.flood_fill_fixed(channel, (1,)*3, (1,)*3)

        if not self.is_find_lane_failed(mask, h/5):
            contours = self.contours_find(image, mask)
            image_with_zone = self.contours_draw(image, contours, saveRequired)
            mask_of_contours = self.contours_to_mask(image, contours, saveRequired=saveRequired)
            self.savetxt(mask_of_contours)
            contours = self.contours_find(image, mask_of_contours)
            # self.contours_draw(image, contours, contourIdx=0, saveRequired=True, operation='printContour')
            self.contour_feature_fitLine(image_with_zone, contours[0])
            return self.save("fitline", image_with_zone)
        else:
            return image

    def process_image_v6_2_11(self, image, saveRequired=False): # add fitline
        self.set_version('v6.2.11')
        h,w = self.get_shape(image)
        gray=self.rgb2gray(image)

        focus_mesure = self.blur_measure(gray)
        print 'focus_mesure: ' +   str(focus_mesure)

        if focus_mesure>200: # image626: 418
            gray = self.blur_gaussian(gray, 5) # kernel_size
        if focus_mesure<50:
            gray = self.sharpen_gamma(gray, 0.8)
        canny = self.canny(gray, 60, 100) # low_threshold, high_threshold
        erode = self.contours_find(image, canny)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)
        erode[ (h-50):h, (w/4): (w/4)*3]=0 # zone not interest
        self.save('roi', erode)
        erode=self.morph_close(erode, kernel_size=(20,20))

        channel=erode
        # channel[0:h/5,0:w]=255 # zone not interest
        contours = self.contours_find(image, channel)
        # lines = np.zeros(image.shape[:2], np.uint8)

        point1, point2 = self.contour_feature_fitLine_2point(image, contours[0])
        point3, point4 = self.contour_feature_fitLine_2point(image, contours[1])

        cross2 = self.GetCrossPoint(point1, point2, point3, point4)
        # self.draw_point(lines, cross2, 20)

        # cv2.line(lines,(w/2, h),cross2,255,2)
        cv2.line(image,(w/2, h),cross2,255,2)

        # self.save("fitline", lines)
        return self.save("fitline", image)

    def process_image_v6_2_12(self, image, saveRequired=False): # add fitline
        self.set_version('v6.2.12')
        h,w = self.get_shape(image)
        gray=self.rgb2gray(image)

        focus_mesure = self.blur_measure(gray)
        print 'focus_mesure: ' +   str(focus_mesure)

        if focus_mesure>200: # image626: 418
            gray = self.blur_gaussian(gray, 5) # kernel_size
        if focus_mesure<50:
            gray = self.sharpen_gamma(gray, 0.8)
        canny = self.canny(gray, 60, 100) # low_threshold, high_threshold
        erode = self.contours_find(image, canny)
        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)
        erode[ (h-50):h, (w/4): (w/4)*3]=0 # zone not interest
        self.save('roi', erode)
        erode=self.morph_close(erode, kernel_size=(20,20))

        channel=erode
        # channel[0:h/5,0:w]=255 # zone not interest
        contours = self.contours_find(image, channel)
        # lines = np.zeros(image.shape[:2], np.uint8)

        self.contours_draw(image, contours, 0)
        self.contours_draw(image, contours, 1)
        point1, point2, k1 = self.contour_feature_fitLine_2point(image, contours[0])
        print 'k1: '+str(-k1)
        point3, point4, k2 = self.contour_feature_fitLine_2point(image, contours[1])
        print 'k2: '+str(-k2)

        cross2 = self.GetCrossPoint(point1, point2, point3, point4)
        # self.draw_point(lines, cross2, 20)

        # cv2.line(lines,(w/2, h),cross2,255,2)
        cv2.line(image,(w/2, h),cross2,255,2)

        # self.save("fitline", lines)
        return self.save("fitline", image)


    def process_image_v6_2_13(self, image, saveRequired=False): # add fitline
        self.set_version('v6.2.13')
        h,w = self.get_shape(image)
        gray=self.rgb2gray(image)

        focus_mesure = self.blur_measure(gray)
        print 'focus_mesure: ' +   str(focus_mesure)

        if focus_mesure>200: # image626: 418
            gray = self.blur_gaussian(gray, 5) # kernel_size
        if focus_mesure<50:
            gray = self.sharpen_gamma(gray, 0.8)
        canny = self.canny(gray, 60, 100) # low_threshold, high_threshold
        erode = self.contours_find(image, canny)

        self.output_file_name+='cntArea'
        for area, cnt in self.contours_feature_area(erode):
            print 'area: ' + str(area)
            self.contours_draw(image, self.contour_to_contours(cnt), operation=None)

        self.output_file_name+='cntLength'
        for length, cnt in self.contours_feature_perimeter(erode):
            print 'length: ' + str(length)
            self.contours_draw(image, self.contour_to_contours(cnt),  operation=None)

        # contour1 = self.contours_filter_area_nth_biggest(contours_area, 1)
        # self.contours_draw(image, self.contour_to_contours(contour1))
        # contour2 = self.contours_filter_area_nth_biggest(contours_area, 2)
        # self.contours_draw(image, self.contour_to_contours(contour2))

        erode = self.contours_filter_size(image, erode, 50, 50)
        erode = self.contours_to_mask(image, erode)
        erode[ (h-50):h, (w/4): (w/4)*3]=0 # zone not interest
        self.save('roi', erode)
        erode=self.morph_close(erode, kernel_size=(20,20))

        channel=erode
        # channel[0:h/5,0:w]=255 # zone not interest
        contours = self.contours_find(image, channel)
        # lines = np.zeros(image.shape[:2], np.uint8)

        self.contours_draw(image, contours, 0)
        self.contours_draw(image, contours, 1)
        point1, point2, k1 = self.contour_feature_fitLine_2point(image, contours[0])
        print 'k1: '+str(-k1)
        point3, point4, k2 = self.contour_feature_fitLine_2point(image, contours[1])
        print 'k2: '+str(-k2)

        cross2 = self.GetCrossPoint(point1, point2, point3, point4)
        # self.draw_point(lines, cross2, 20)

        # cv2.line(lines,(w/2, h),cross2,255,2)
        cv2.line(image,(w/2, h),cross2,255,2)

        # self.save("fitline", lines)
        return self.save("fitline", image)

    def process_image_v6_3_1(self, image, saveRequired=False): # fix file_holes using edge_replace
        self.set_version('v6.3.1')
        h,w = self.get_shape(image)
        gray=self.rgb2gray(image)

        # adaptive=self.threshold_adaptive(gray, 29, 1)
        threshold=self.threshold(gray, 170)
        # threshold=self.threshold(gray, 150)
        # threshold=self.threshold(gray, 195)

        # img_hsv = self.rgb2hsv(image)
        # mask_yw = self.mask_to_yellow_or_white(gray, img_hsv)
        # mask_yw_image = self.bitwise_and(gray, mask_yw)
        #
        # gauss_gray = self.blur_gaussian(mask_yw_image, 5) # kernel_size
        #
        # canny_edges = self.canny(gauss_gray, 50, 150) # low_threshold, high_threshold

        threshold[0:h/5,0:w]=0 # zone not interest
        erode=self.morph_erode(threshold, kernel_size=(3,3))
        contours = self.contours_find(image, threshold)
        contours_lengths = self.contours_feature_perimeter(contours)

        positiveLine = None
        negativeLine = None
        for length, cnt in contours_lengths:
            if length<100:
                continue
            # self.contours_draw(image, self.contour_to_contours(cnt),  operation=None)
            copy = image.copy()
            point1, point2, k = self.contour_feature_fitLine_2point(image, cnt)
            cv2.line(copy,point1,point2,255,2)
            self.save("fitline", copy, saveRequired=saveRequired)
            (a, b, c) = self.GetLinePara(point1, point2)
            k2 = -float(a)/float(b)
            jieju = abs(float(c)/float(b))
            print 'length: ' + str(length) + ", k temp: " + str(k) + ", k calculate: " + str(k2) + ", jieju: "+ str(jieju)

            if k>0 and (positiveLine is None or positiveLine[3]< jieju):
                positiveLine = (point1, point2, k, jieju)
            if k<0 and (negativeLine is None or negativeLine[3]< jieju):
                negativeLine = (point1, point2, k, jieju)
            if positiveLine is not None and negativeLine is not None:
                break

        print "\npositiveLine: " + str(positiveLine)
        print "negativeLine: " + str(negativeLine)
        if positiveLine is None and negativeLine is not None:
            cv2.line(image,negativeLine[0],negativeLine[1],255,2)
            k = negativeLine[2]
        if positiveLine is not None and negativeLine is None:
            cv2.line(image,positiveLine[0],positiveLine[1],255,2)
            k = positiveLine[2]
        if positiveLine is not None and negativeLine is not None:
            if abs(positiveLine[2]-negativeLine[2])<1 and abs(positiveLine[3]-negativeLine[3])<40:
                cv2.line(image,positiveLine[0],positiveLine[1],255,2)
                k = positiveLine[2]
            else:
                cross2 = self.GetCrossPoint(negativeLine[0], negativeLine[1], positiveLine[0], positiveLine[1])
                # self.draw_point(lines, cross2, 20)
                (a, b, c) = self.GetLinePara((w/2, h), cross2)
                print "a="+str(a)+", b="+str(b)+", c="+str(c)
                k = -float(a)/float(b)
                # cv2.line(lines,(w/2, h),cross2,255,2)
                cv2.line(image,negativeLine[0],negativeLine[1],127,1)
                cv2.line(image,positiveLine[0],positiveLine[1],127,1)
                cv2.line(image,(w/2, h),cross2,255,2)


        degree = int(degrees(atan(-k)))

        if degree<0:
            degree+=180
        if degree>180:
            degree-=180
        bracage = 1.3*degree
        pwm = int(270+bracage)
        # todo: min pwm and max pwm
        print "k = " + str(k) + ", degree = " + str(degree) + ", pwm = "+ str(pwm)

        return (self.save("result", image, saveRequired=False), degree, pwm)


if __name__ == '__main__':
    it = image_tool(debug=False)

    # it.example()
    # it.process_image_v1(image, True)
    # it.process_image_v2(image, True)
    # it.process_image_v5(image, True)
    # it.process_video_v5('solidWhiteRight.mp4')
    # it.process_image_v3_3_1(None) # image8_text.jpg adaptiveThresh
    # it.process_image_v3_5(None)
    # it.process_image_v3_7(None)
    for file_name in [
        # 'image6_text.png',
        # 'image7_text.png',
        # 'image8_text.jpg',







        # 'image5_shake.png',
        # 'image621.png',
        # 'image622.png',
        # 'image626.png',
        # 'image630.png',
        # 'image6212.png',
        'image1.png',
        'image2.png',
        'image3.png',
        'image4_shadow.png',
        'image623.png',
        'image627.png',

        'image628.png',

        ]:
        image = it.imread("../20181215/" + file_name)
        # it.process_image_v3_1(image, True)
        # it.process_image_v3_2(image, True)
        # it.process_image_v3_3(image, True)
        # it.process_image_v3_4(image, True) # image7_text.png theshIntegral
        # it.process_image_v3_6(image, True) # image7_text.png theshIntegral

        # it.process_image_v4_0(image, True)
        # it.process_image_v4_1(image, True)
        # it.process_image_v4_2(image, True)
        # it.process_image_v4_4(image, True)
        # it.process_image_v6_2_3(image, True)
        # it.process_image_v6_2_9(image, True)
        # it.process_image_v6_2_11(image, True)
        # it.process_image_v6_2_12(image, True)
        # it.process_image_v6_2_13(image, True)
        it.process_image_v6_3_1(image, False)


    sys.exit(0)