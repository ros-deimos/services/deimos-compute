#!/usr/bin/env python

#Streaming video render in new topic
import time
import rospy
import sys
import cv2
import numpy as np
from sensor_msgs.msg import CompressedImage


class StreamingPub:
  def __init__(self):
    '''Initialize ros publisher send PWM steering msg'''
    self.image_pub = rospy.Publisher("/compute/debug/image", CompressedImage, queue_size = 1)
    #rospy.init_node('SteeringPub')

  def update(self, streaming):
    # Show it
    #cv2.imshow("image_axis", encoded_img)
    #cv2.waitKey(0) # Press 'q' to exit

    # grab the dimensions to calculate the center
    (h, w) = streaming.shape[:2]
    center = (w / 2, h / 2)
    # rotate the image by 180 degrees
    M = cv2.getRotationMatrix2D(center, 180, 1.0)
    rotated = cv2.warpAffine(streaming, M, (w, h))

    #cv2.imshow('cv_img', streaming)
    #cv2.waitKey(2)

    msg = CompressedImage()
    msg.header.stamp = rospy.Time.now()
    msg.format = "jpeg"
    msg.data = np.array(cv2.imencode('.jpg', streaming)[1]).tostring()
    self.image_pub.publish(msg)
