#!/usr/bin/env python

#from Adafruit_PWM_Servo_Driver import PWM
import time
import rospy
import sys
from std_msgs.msg import Int16

class ThrottlePub(object):
  def __init__(self):
    '''Initialize ros publisher send PWM trottle msg'''
    self.pub = rospy.Publisher("/compute/pwm/speed", Int16, queue_size=10)
    self.MAX_LEFT_PWM = 360
    self.MAX_RIGHT_PWM = 500

  def update(self, trottle):
   pwm = int(trottle)
   self.pub.publish(pwm)
