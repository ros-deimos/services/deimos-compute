#!/usr/bin/env python

#from Adafruit_PWM_Servo_Driver import PWM
import time
import rospy
import sys
from std_msgs.msg import Int16

class SteeringPub(object):
  def __init__(self):
    '''Initialize ros publisher send PWM steering msg'''
    self.pwm = rospy.Publisher("/compute/pwm/steering", Int16, queue_size=10)
    self.angle = rospy.Publisher("/compute/angle/degree", Int16, queue_size=10)
    self.MAX_LEFT_PWM = 490
    self.MAX_RIGHT_PWM = 270

  def update(self, steering):
   angle = int(steering)
   bracage = 1.3*angle
   pwm = int(270+bracage)
   print("degrees: ", angle, "steering_pwm: ", pwm)
   self.pwm.publish(pwm)
   self.angle.publish(angle)
