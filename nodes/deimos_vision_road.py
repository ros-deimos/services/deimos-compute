#!/usr/bin/env python
"""OpenCV feature detectors with ros CompressedImage Topics in python.

This example subscribes to a ros topic containing sensor_msgs
CompressedImage. It converts the CompressedImage into a numpy.ndarray,
then detects and marks features in that image. It finally displays
and publishes the new image - again as CompressedImage topic.

Ref: http://wiki.ros.org/rospy_tutorials/Tutorials/WritingImagePublisherSubscriber
"""
__author__ =  'Jerome ZAMAROCZY <jerome.zamaroczy@renault.com>'
__version__=  '0.1'
__license__ = 'BSD'

import cv2
import numpy as np
import math
from math import acos, degrees
import rospy
import sys
from sensor_msgs.msg import CompressedImage
import imutils

# show opencv version
(major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
print "OpenCV Major version :  {0}".format(major_ver)
print "OpenCV Minor version :  {0}".format(minor_ver)
print "OpenCV Submior version :  {0}".format(subminor_ver)
VERBOSE=False

class deimos_vision:

    def __init__(self):
        '''Initialize ros publisher, ros subscriber'''
        self.image_pub = rospy.Publisher("/opencv/detect/road", CompressedImage, queue_size = 1)
        self.subscriber = rospy.Subscriber("/usb_cam/image_raw/compressed", CompressedImage, self.callback,  queue_size = 1)
        if VERBOSE :
            print "subscribed to /camera/image/compressed"

    def distance(self, (x1, y1), (x2,y2)):
        dist = math.sqrt((math.fabs(x2-x1))**2+((math.fabs(y2-y1)))**2)
        return dist

    def angle(self, dst_A, dst_B, dst_C):
        compute_angle = degrees(acos((dst_A * dst_A + dst_B * dst_B - dst_C * dst_C)/(2.0 * dst_A * dst_B)))
        return compute_angle

    def displayHorizonLine(self, image):
        height = np.size(image, 0)
        width = np.size(image, 1)
        hmin = 0, height/2
        hmax = width, height/2
        center = width/2, height/2
        draw_center = cv2.circle(image, center, 6, (255,50,50), -1)
        draw_horizon = cv2.line(image, hmin, hmax, (255,50,50), 1, 8, 0)

    def displayVerticalLine(self, image):
        height = np.size(image, 0)
        width = np.size(image, 1)
        footer_center = width/2, height
        tooper_center = width/2, 0
        draw_vertical = cv2.line(image, footer_center, tooper_center, (255,50,50), 1, 8, 0)

    def displayTriangular(self, image, rotate = int(0)):
        height = np.size(image, 0)
        width = np.size(image, 1)
        center = width/2, height/2
        #Calcul Matrice de rotation
        theta = np.radians(rotate)
        c, s = np.cos(theta), np.sin(theta)
        R = np.matrix('{} {}; {} {}'.format(c, s, -s, c))
        pt1 = np.array((-20, 0))
        pt2 = np.array((0, 20))
        pt3 = np.array((0, -20))
        mar1 = R.dot(pt1)
        mar2 = R.dot(pt2)
        mar3 = R.dot(pt3)
        pt1 = (int(mar1.item(0))+width/2, int(mar1.item(1))+height/2)
        pt2 = (int(mar2.item(0))+width/2, int(mar2.item(1))+height/2)
        pt3 = (int(mar3.item(0))+width/2, int(mar3.item(1))+height/2)
        #pt1 is Top direction in red color cercle
        cv2.circle(image, pt1, 4, (0,0,255), -1)
        #cv2.circle(image, pt2, 2, (0,0,255), -1)
        #cv2.circle(image, pt3, 2, (0,0,255), -1)
        triangle_cnt = np.array( [pt1, pt2, pt3] )
        cv2.drawContours(image, [triangle_cnt], 0, (0,255,0), -1)


    def draw_lane(self, image, cX, cY, perimeter, area, c):
        lane = cv2.circle(image, (cX, cY), 2, (0, 255, 0), -1)
        contours = cv2.drawContours(lane, [c], -1, (0, 0, 255), 2)
        rows,cols = image.shape[:2]
        [vx,vy,x,y] = cv2.fitLine(c, cv2.DIST_L2,0,0.01,0.01)
        lefty = int((-x*vy/vx) + y)
        righty = int(((cols-x)*vy/vx)+y)
        height = np.size(image, 0)
        width = np.size(image, 1)
        footer_center = width/2, height
        center = width/2, height/2
        footer_left = 0, height
        #Left section and detect only the center surface line white
        if cX < width/2 and cY < height/2:
            dst_A = self.distance(footer_center,(cols-1,righty))
            dst_B = self.distance(footer_left,footer_center)
            dst_C = self.distance(footer_left,(cols-1,righty))
            turn_angle = self.angle(dst_A, dst_B, dst_C)
            self.displayTriangular(image, int(-turn_angle))
        #Right section and detect only the center surface line white
        if cX > width/2 and cY < height/2:
            dst_A = self.distance(footer_center,(0,lefty))
            dst_B = self.distance(footer_left,footer_center)
            dst_C = self.distance(footer_left,(0,lefty))
            turn_angle = self.angle(dst_A, dst_B, dst_C)
            self.displayTriangular(image, int(-turn_angle))
        return lane

    def callback(self, ros_data):
        '''Callback function of subscribed topic.
        Here images get converted and features detected'''
        if VERBOSE :
            print 'received image of type: "%s"' % ros_data.format
        np_arr = np.fromstring(ros_data.data, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR) # OpenCV >= 3.0:
        # Filter and mask reduce noise
        flt_color_hls = cv2.cvtColor(image_np, cv2.COLOR_RGB2HLS)
        flt_color_gray = cv2.cvtColor(flt_color_hls, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(flt_color_gray, (1, 5), 2)
        thresh = cv2.threshold(blurred, 112, 165, cv2.THRESH_BINARY)[1]

        contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
	    cv2.CHAIN_APPROX_SIMPLE)
        cnts = contours[0] if imutils.is_cv2() else contours[1]
        for c in cnts:
            M = cv2.moments(c)
            area = cv2.contourArea(c)
            perimeter = cv2.arcLength(c,True)
            epsilon = 0.1*cv2.arcLength(c,True)
            approx = cv2.approxPolyDP(c,epsilon,True)
            if M["m00"] != 0:
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                if (1000 > perimeter > 500) and (5500 > area > 2000):
                    image_np = self.draw_lane(image_np, cX, cY, perimeter, area, c)
            else:
                cX, cY = 0, 0
        self.displayHorizonLine(image_np)
        self.displayVerticalLine(image_np)
        cv2.imshow('cv_img', image_np)
        cv2.waitKey(2)

        msg = CompressedImage()
        msg.header.stamp = rospy.Time.now()
        msg.format = "jpeg"
        msg.data = np.array(cv2.imencode('.jpg', image_np)[1]).tostring()
        self.image_pub.publish(msg)

def main(args):
    '''Initializes and cleanup ros node'''
    ic = deimos_vision()
    rospy.init_node('image_feature', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down ROS Image feature detector module"
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
