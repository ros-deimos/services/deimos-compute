#!/usr/bin/env python

import cv2
import numpy as np
import math
from math import acos, degrees
import rospy
import sys
from sensor_msgs.msg import CompressedImage
import imutils

class DeisCompute:

    def __init__(self):
        self.c = 0
        self.x = 0
        self.y = 0
        self.perimeter = 0
        self.area = 0
        self.image = 0

    def distance(self, (x1, y1), (x2,y2)):
        dist = math.sqrt((math.fabs(x2-x1))**2+((math.fabs(y2-y1)))**2)
        return dist

    def angle(self, dst_A, dst_B, dst_C):
        compute_angle = degrees(acos((dst_A * dst_A + dst_B * dst_B - dst_C * dst_C)/(2.0 * dst_A * dst_B)))
        return compute_angle

    def direction(self):
        image = self.image
        rows,cols = image.shape[:2]
        [vx,vy,x,y] = cv2.fitLine(self.c, cv2.DIST_L2,0,0.01,0.01)
        lefty = int((-x*vy/vx) + y)
        righty = int(((cols-x)*vy/vx)+y)

        if self.x < self.width/2 and self.y < self.height/2:
            dst_A = self.distance(self.footer_center,(cols-1,righty))
            dst_B = self.distance(self.footer_left,self.footer_center)
            dst_C = self.distance(self.footer_left,(cols-1,righty))
            turn_angle = self.angle(dst_A, dst_B, dst_C)
            return turn_angle

        #Right section and detect only the center surface line white
        elif self.x > self.width/2 and self.y < self.height/2:
            dst_A = self.distance(self.footer_center,(0,lefty))
            dst_B = self.distance(self.footer_left,self.footer_center)
            dst_C = self.distance(self.footer_left,(0,lefty))
            turn_angle = self.angle(dst_A, dst_B, dst_C)
            return turn_angle

        else:
            turn_angle = 90
            return turn_angle



    def moment(self, image):
        self.image = image
        contours = cv2.findContours(self.image, cv2.RETR_EXTERNAL,
	    cv2.CHAIN_APPROX_SIMPLE)
        cnts = contours[0] if imutils.is_cv2() else contours[1]
        for c in cnts:
            M = cv2.moments(c)
            area = cv2.contourArea(c)
            perimeter = cv2.arcLength(c,True)
            epsilon = 0.1*cv2.arcLength(c,True)
            approx = cv2.approxPolyDP(c,epsilon,True)

            if M["m00"] != 0:
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                self.x = cX
                self.y = cY
                self.perimeter = perimeter
                self.area = area
                self.c = c
            else:
                cX, cY = 0, 0

    def sizewindow(self):
        height = np.size(self.image, 0)
        width = np.size(self.image, 1)
        self.height = height
        self.width = width
        self.footer_center = width/2, height
        self.footer_left = 0, height
        self.tooper_center = width/2, 0
        self.hmin = 0, height/2
        self.hmax = width, height/2
        self.center = width/2, height/2
