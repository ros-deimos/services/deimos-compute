#!/usr/bin/env python
"""OpenCV feature detectors with ros CompressedImage Topics in python.

This example subscribes to a ros topic containing sensor_msgs 
CompressedImage. It converts the CompressedImage into a numpy.ndarray, 
then detects and marks features in that image. It finally displays 
and publishes the new image - again as CompressedImage topic.

Ref: http://wiki.ros.org/rospy_tutorials/Tutorials/WritingImagePublisherSubscriber
"""
__author__ =  'Simon Haller <simon.haller at uibk.ac.at>'
__version__=  '0.1'
__license__ = 'BSD'

# OpenCV
import cv2
# numpy and scipy
import numpy as np
# Ros libraries
import rospy
# Python libs
import sys
from image_tool import image_tool
# Ros Messages
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import Int16

# We do not use cv_bridge it does not support CompressedImage in python
# from cv_bridge import CvBridge, CvBridgeError

# show opencv version
(major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
print "OpenCV Major version :  {0}".format(major_ver)
print "OpenCV Minor version :  {0}".format(minor_ver)
print "OpenCV Submior version :  {0}".format(subminor_ver)

VERBOSE=False

class image_feature:

    def __init__(self):
        '''Initialize ros publisher, ros subscriber'''
        # topic where we publish
        self.pwm = rospy.Publisher("/compute/pwm/steering", Int16, queue_size=10)
        self.angle = rospy.Publisher("/compute/angle/degree", Int16, queue_size=10)

        self.image_pub = rospy.Publisher("/output/image_raw/compressed", CompressedImage, queue_size = 1)
        self.it = image_tool()
        # self.bridge = CvBridge()

        # self.example()



        # subscribed Topic
        self.subscriber = rospy.Subscriber("/usb_cam/image_raw/compressed", CompressedImage, self.callback,  queue_size = 1)
        if VERBOSE :
            print "subscribed to /camera/image/compressed"

    def callback(self, ros_data):
        '''Callback function of subscribed topic. 
        Here images get converted and features detected'''
        if VERBOSE :
            print 'received image of type: "%s"' % ros_data.format

        #### direct conversion to CV2 ####
        np_arr = np.fromstring(ros_data.data, np.uint8)
        #image_np = cv2.imdecode(np_arr, cv2.CV_LOAD_IMAGE_COLOR)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR) # OpenCV >= 3.0:

        # image_np = self.it.process_image_v6_2_3(image_np) # only not good for shake
        # image_np = self.it.process_image_v6_2_4(image_np) # fast
        (image_np, angle, pwm) = self.it.process_image_v6_3_1(image_np)


        # image_np = self.it.process_image_v6_4(image_np) # best
        # image_np = self.it.process_image_v6_6(image_np)

        # show it
        cv2.imshow('cv_img', image_np)
        cv2.waitKey(2)

        #### Create CompressedIamge ####
        msg = CompressedImage()
        msg.header.stamp = rospy.Time.now()
        msg.format = "jpeg"
        msg.data = np.array(cv2.imencode('.jpg', image_np)[1]).tostring()

        # Publish new image
        self.image_pub.publish(msg)
        self.pwm.publish(pwm)
        self.angle.publish(angle)
        
        #self.subscriber.unregister()


def main(args):
    '''Initializes and cleanup ros node'''
    ic = image_feature()
    rospy.init_node('image_feature', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down ROS Image feature detector module"
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)