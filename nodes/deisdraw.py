#!/usr/bin/env python

import cv2
import numpy as np
import math
from math import acos, degrees
import rospy
import sys
from sensor_msgs.msg import CompressedImage
import imutils

class DeisDraw:

    def __init__(self):
        self.topic = "/usb_cam/image_raw/compressed"
        self.image = rospy.Subscriber("/usb_cam/image_raw/compressed", CompressedImage, self.decode,  queue_size = 1)

    def origin(self):
        return self.live_image

    def render(self):
        return self._final_image

    def decode(self, data_topic):
        #print 'received image of type: "%s"' % data_topic.format
        np_arr = np.fromstring(data_topic.data, np.uint8)
        self.live_image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

    def refresh(self):
        self.origin = self.live_image
        self._tmp_image = self.live_image
        self._final_image = self.live_image

    def grey(self):
        self._tmp_image =  cv2.cvtColor(
            self._tmp_image,
            cv2.COLOR_BGR2GRAY
        )
        self._final_image = self._tmp_image

    def hls(self):
        self._tmp_image =  cv2.cvtColor(
            self._tmp_image,
            cv2.COLOR_RGB2HLS
        )
        self._final_image = self._tmp_image

    def gaussian(self):
        self._tmp_image = cv2.GaussianBlur(
            self._tmp_image,
            (1, 5),
            2
        )
        self._final_image = self._tmp_image

    def threshold(self):
        self._tmp_image = cv2.threshold(
            self._tmp_image,
            112, 165,
            cv2.THRESH_BINARY
        )[1]
        self._final_image = self._tmp_image


    def horizonLine(self, center, hmin, hmax):
        self.origin =  cv2.circle(
            self.origin,
            center,
            6,
            (255,50,50),
            -1
        )
        self.final = self.origin

        self.origin =  cv2.line(
            self.origin,
            hmin,
            hmax,
            (255,50,50),
            1,
            8,
            0
        )
        self._final_image = self.origin

    def verticalline(self, footer_center, tooper_center):
        self.origin =  cv2.line(
            self.origin,
            footer_center,
            tooper_center,
            (255,50,50),
            1,
            8,
            0
        )
        self._final_image = self.origin

    def lineroad(self, cX, cY, perimeter, area, c):
        if (1000 > perimeter > 500) and (5500 > area > 2000):
            self.origin = cv2.circle(self.origin, (cX, cY), 2, (0, 255, 0), -1)
            ctr1 = self.origin
            contours = cv2.drawContours(ctr1, [c], -1, (0, 0, 255), 2)
            self._final_image = self.origin

    def TriangularGeo(self, rotate, width, height):
        rotate = int(rotate)
        width = int(width)
        height = int(height)
        
        theta = np.radians(-rotate)
        c, s = np.cos(theta), np.sin(theta)
        R = np.matrix('{} {}; {} {}'.format(c, s, -s, c))
        pt1 = np.array((-20, 0))
        pt2 = np.array((0, 20))
        pt3 = np.array((0, -20))
        mar1 = R.dot(pt1)
        mar2 = R.dot(pt2)
        mar3 = R.dot(pt3)
        pt1 = (int(mar1.item(0))+width/2, int(mar1.item(1))+height/2)
        pt2 = (int(mar2.item(0))+width/2, int(mar2.item(1))+height/2)
        pt3 = (int(mar3.item(0))+width/2, int(mar3.item(1))+height/2)
        self.origin = cv2.circle(
            self.origin,
            pt1,
            4, (0,0,255), -1
        )
        #cv2.circle(image, pt2, 2, (0,0,255), -1)
        #cv2.circle(image, pt3, 2, (0,0,255), -1)
        triangle_cnt = np.array( [pt1, pt2, pt3] )
        self.origin = cv2.drawContours(
            self.origin,
            [triangle_cnt],
            0, (0,255,0), -1
        )
        self._final_image = self.origin
