#!/usr/bin/env python

import cv2
import numpy as np
import math
from math import acos, degrees
import rospy
import sys
from sensor_msgs.msg import CompressedImage
import imutils
from deisdraw import DeisDraw
from deiscompute import DeisCompute
from steeringpub import SteeringPub
from throttlepub import ThrottlePub
from streamingpub import StreamingPub
#from PIL import Image
import signal

def signal_handler(signal, frame):
    print("\nprogram exiting gracefully")
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

def main(args):
    '''Initializes and cleanup ros node'''
    stream = DeisDraw()
    compute = DeisCompute()
    steering = SteeringPub()
    throttle = ThrottlePub()
    streaming = StreamingPub()
    rospy.init_node('image_feature', anonymous=True)
    timebreak = 90
    while True:
        stream.refresh()
        stream.hls()
        stream.grey()
        stream.gaussian()
        stream.threshold()
        compute.moment(stream.render())
        compute.sizewindow()
        bracage = compute.direction()

        if (1000 > int(compute.perimeter) > 500) and (5500 > int(compute.area) > 2000):
            stream.TriangularGeo(bracage, compute.width, compute.height)
            steering.update(bracage)
            throttle.update(390)

            timebreak = bracage

        else:
            stream.TriangularGeo(timebreak, compute.width, compute.height)

        throttle.update(420)
        #print compute.x, compute.y, compute.perimeter, compute.area
        #print compute.center, compute.width, compute.height
        stream.verticalline(compute.footer_center, compute.tooper_center)
        stream.horizonLine(compute.center, compute.hmin, compute.hmax)
        stream.lineroad(compute.x, compute.y, compute.perimeter, compute.area, compute.c)
        streaming.update(stream.render())
        #cv2.imshow('cv_img', stream.render())
        #cv2.waitKey(2)

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting down ROS Image feature detector module"
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
