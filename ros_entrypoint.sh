#!/bin/bash
set -e

# setup ros environment
source "/opt/ros/$ROS_DISTRO/setup.bash"

if [ -f ./devel/setup.sh ]; then
    source "./devel/setup.sh"
    source "./devel/setup.bash"
fi


exec "$@"
