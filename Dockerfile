FROM ros:kinetic

ENV WORKSPACE_NAME="catkin"
RUN mkdir -p /usr/ros-package/${WORKSPACE_NAME}
WORKDIR /usr/ros-package/${WORKSPACE_NAME}

# Install Dev Mode
RUN (apt-get update && \
     apt-get install -y -f build-essential git curl wget && \
     apt-get install -y --no-install-recommends wget unzip git python cmake vim python-pip && \
     apt-get install -y ros-kinetic-image-transport \
                        ros-kinetic-compressed-image-transport \
                        ros-kinetic-compressed-depth-image-transport \
                        ros-kinetic-image-transport-plugins \
                        ros-$ROS_DISTRO-catkin)

RUN (pip install opencv-python \
                 imutils)

# Add and build app
RUN mkdir ./src 
ADD . ./src
COPY ./ros_entrypoint.sh /
RUN chmod +x /ros_entrypoint.sh
RUN /bin/bash -c ". /opt/ros/${ROS_DISTRO}/setup.bash; catkin_make"