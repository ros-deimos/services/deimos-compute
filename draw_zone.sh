#!/bin/bash

roscore &
cd ~/catkin_ws/src/opencv_vision_pkg/20181215
echo 'rosrun deimos_compute draw_zone_v2.py'
rosrun rqt_image_view rqt_image_view &

rosbag play --loop --pause 2018-12-15-02-07-00.bag
#rosbag play --loop --pause  --start 5 --duration 5 --rate 1 2018-12-15-02-07-00.bag
