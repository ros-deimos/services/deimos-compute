# deimos_vision_ros
### Opencv nodes publish PWM steering by compute line border

![img](img/screenshot.png)

* Set and export URI ROS MASTER NODE TARGET
> export ROS_MASTER_URI=http://localhost:11311

Install package ROS:
> apt-get install ros-kinetic-usb-cam

Stream Video Bag:
> while true; do rosbag play 2018-12-15-02-11-36.bag; sleep .5; done

View Render:
> rosrun rqt_image_view rqt_image_view

init node :
> rosrun deimos_vision main.py
